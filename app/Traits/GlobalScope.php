<?php


namespace App\Traits;


trait GlobalScope
{
    public function scopeIsDealer($query)
    {
        return $query->where('defined_user', 'dealer');
    }

    public function scopeIsActive($query, $value)
    {
        return $query->where('is_active', $value);
    }

    public function scopeIsAccepted($query , $value)
    {
        return $query->where('is_accepted', $value);
    }

    public function scopeIsSuspend($query , $value)
    {
        return $query->where('is_suspend', $value);
    }

    public function scopeIsDeleted($query , $value)
    {
        return $query->where('is_deleted',$value);
    }

    public function scopeIsPayed($query, $value)
    {
        return $query->where('is_payed',$value);
    }
}
