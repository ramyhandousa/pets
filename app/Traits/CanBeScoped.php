<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 3:12 PM
 */

namespace App\Traits;

use App\Scoping\Scoper;
use Illuminate\Database\Eloquent\Builder;

trait CanBeScoped
{

    public function scopeWithScopes(Builder $builder, $scopes = [])
    {
        return ( new Scoper(request()) )->apply($builder, $scopes);
    }

}
