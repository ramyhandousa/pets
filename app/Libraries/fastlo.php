<?php


namespace App\Libraries;


class fastlo
{
    public $api_key ;
    public $basURL;
    public $country;

    //status_code = 10 (New)
    //status_code = 20 (Pickup In Progress)
    //status_code = 30 (Picked Up)
    //status_code = 40 (In Distribution Center)
    //status_code = 50 (Shipping In Progress)
    //status_code = 60 (Delivery In Progress)
    //status_code = 70 (Canceled)
    //status_code = 80 (Returned)
    //status_code = 90 (Dispatched)
    //status_code = 100 (Delivered)

    public function __construct()
    {
        $this->api_key = "cebbc008d7fb5eec1add5aaaea019442pqet07ju8tc1q0n01zlhau10cbfh4t06";

        $this->basURL = "https://fastlo.com/api/v1";

        $this->country = "SA";
    }

    function list_cities(){

        return $this->fastloCall("/pickup_cities", ['country' => $this->country]);
    }


    function  code_cities(){

        return $this->fastloCall("/cod_cities", ['country' => $this->country]);
    }

    function  prices_shipment(){

        return $this->fastloCall("/prices_shipment",['delivery' => 1, 'shipping' => 1]);
    }

    function addShipment($senderAddress, $receiverAddress, $shipmentData) {
        $data = array(
            'sender_address' => $senderAddress,
            'receiver_address' => $receiverAddress,
            'shipment_data' => $shipmentData
        );
        return $this->fastloCall("/add_shipment",$data);
    }

    function readShipment($trackNumber) {

          return $this->fastloCall("/read_shipment",['tracknumber' => $trackNumber]);
	}

    function getShipmentsStatus($trackNumbers ) {

        if (!is_array($trackNumbers)) {
            $trackNumbers = explode(',', $trackNumbers);
        }

        $data = array('tracknumbers_list' => $trackNumbers);

        return $this->fastloCall("/status_shipments",$data);
    }


    function canBeCanceled($trackNumber ) {

        return $this->fastloCall("/can_cancel_shipment",['tracknumber' => $trackNumber]);
    }

    function cancelShipment($trackNumber ) {

        return $this->fastloCall("/cancel_shipment",['tracknumber' => $trackNumber]);
    }

    function fastloCall($method, $data) {

        $url = $this->basURL.$method;
        $request = json_encode(array('request' => $data), JSON_UNESCAPED_UNICODE);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('fastlo-api-key: '.$this->api_key, 'Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($ch);
        curl_close($ch);

        if ($method == 'label_shipment' && $data['pdf_format'] == 'binary') {
            header('Content-type: application/pdf');
            echo $result;
        }
        else {
            $result = json_decode($result, true);
            if (!is_array($result) || !isset($result['status_code'])) {
                $result = array();
                $result['status_code'] = 404;
                $result['error'] = 'Not Found Or Server Error';
            }
            return $result;
        }
    }




}
