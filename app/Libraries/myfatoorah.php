<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 2/24/2020
 * Time: 6:40 PM
 */

namespace App\Libraries;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class myfatoorah
{

    public $headerApiToken;
    public $token;

    public $basURL;
    public $initiatePayment;
    public $executePayment;

    public $getPaymentStatus;
    public $callBackUrl;
    public $errorUrl;

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? : 'ar';
        app()->setLocale($language);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ?   : ' ';

//        $this->token = '_i5_JFz1C7G-wszN362cUUEQb8omq5NjtnQRyuaIk2V9l_0XhENTy4AiAnBMvdxGEzoBNvPSkLoEQzZh-BxQ_QMndwzDuHl2Y_FhwuyzzvrD4nXl0Cxbiau_FvZjzljIP8jlIWkp0cXRD9649piSRSIhDlBnpw3ZpfZjxe4a0_xOqxVxwDY5R0UvfDuFm7Oc0gUc1mLfKK8a4SCA9QJ1JJlOHZBAz15kFiBB-OqDD2F7oKovL1K76bphBfha0KOV5zZsXHx84hGgzxgRTXKxPGVqpBRsHgGCin8CfRKA8lxm1Ts2bzL8s_LUvM7OqEXKQat7yzbH94Ql-rEzfkR_Ih90aU8wHg8dE51cKeBk3APZToPiOdFlddB3AT2lKdriQ17U1oGh2P8RVNzUGv_aP8yPCJViS6bM-ENq7ndOTOwHKaDrNEksPgqCl6GQZE9qFQZBvNKPRk2af2fxDOBNTvaLuoB3SGh2wSECZ4nELr-tPW6s79Vsl3VdfEr0WUY1iji-AhZUkXOnX1TeNDJUB6cIgfmxeObcUBH3O7L8zGKccYaZSbKIisKs7ho-TSj-Mah4OIVXr2u_46Zo60D8AxtC4CJL-mKMSEwyecY-nWOLAJf9aEEDNUr-5ZQLJxah9L0CW1Vn4rt0N0IaTGDBNbJPz91pyOpV0oOUcKJZgtDaRk03PiUNl4dATCD-eTiFvsdknw';
        $this->token = '7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV';

//        $this->basURL           = 'https://api.myfatoorah.com';
        $this->basURL           = 'https://apitest.myfatoorah.com';
        $this->initiatePayment  = $this->basURL . '/v2/InitiatePayment';
        $this->executePayment   = $this->basURL . '/v2/ExecutePayment';
        $this->getPaymentStatus  = $this->basURL . '/v2/GetPaymentStatus';
        $this->callBackUrl      = request()->root() . '/payments/success?orderId=' . \request()->orderId;
        $this->errorUrl         =  request()->root() . '/payments/errors?orderId=' . \request()->orderId;
    }


    public function listPaymentMethod(Request $request){

//        $user = Auth::user();
        $user = User::where('api_token',$this->headerApiToken)->first();

        $fileds = $this->fields($user,$request,$this->initiatePayment);

        $response = $this->myfatoorahResponse($this->initiatePayment,$fileds);

        return $response;
    }

    public function excutePaymentMethod(Request $request){

//        $user = Auth::user();
        $user = User::where('api_token',$this->headerApiToken)->first();
        $fileds = $this->fields($user,$request,$this->executePayment);

         $response = $this->myfatoorahResponse($this->executePayment,$fileds);

        return $response;
    }
    public function getStatus(Request $request){

        $user = User::where('api_token',$this->headerApiToken)->first();


        $fileds = $this->fields($user,$request,$this->getPaymentStatus);



        $response = $this->myfatoorahResponse($this->getPaymentStatus,$fileds);

        return $response;
    }
    public function myfatoorahResponse($curlUrl , $fields ){

         $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlUrl,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $this->token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results =  json_decode($response, true);

        return $results;
    }

    public function fields($user , $request , $curlUrl){

        if ($curlUrl === $this->executePayment) {

            $fields = '{
              "PaymentMethodId": ' . $request->paymentMethodId . ',
              "orderId": ' . $request->orderId . ',
              "CustomerName": "' . $user->name . '",
              "MobileCountryCode": "+966",
              "CustomerMobile": "' . $user->phone . '",
//              "CustomerEmail": "' . $user->email . '",
              "InvoiceValue": ' . $request->total . ',
              "CallBackUrl": "' . $this->callBackUrl . '",
              "ErrorUrl": "' . $this->errorUrl . '",
              "Language": "en",
              "CustomerAddress": {
                "Block": "",
                "Street": "",
                "HouseBuildingNo": "",
                "Address": "' . $user->address . '",
                "AddressInstructions": ""
              },
              
              "SupplierCode": 0,
              "InvoiceItems": [
                {
                  "ItemName": "Service",
                  "Quantity": 1,
                  "UnitPrice": ' . $request->total . '
                }
              ]
            }';

        }elseif($curlUrl === $this->getPaymentStatus){

            $fields = "{  \"keyType\":  \"PaymentId\"  ,\"key\": \"$request->paymentId\"}";

        }else{

            $fields = "{\"InvoiceAmount\": $request->total  ,\"CurrencyIso\": \"SAR\"}";

        }

        return $fields;
    }

    public function success(Request $request){

        return response()->json([
            'status' => 200,
            'message'=> "Success Payment.",
            'data' => $request->all()
        ], 200);
    }

    public function errors(Request $request){

        return response()->json([
            'status' => 400,
            'message'=> "Error Payment.",
            'error' =>  (array)$request->all()
        ], 400);
    }

}