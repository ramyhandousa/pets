<?php


namespace App\Repositories;

use App\Models\Pet;
use App\Scoping\Scopes\typeScope;

class PetRepository
{

    public function list($request){

        $user = $request->user();
        $query =  Pet::whereDefinition('animal')->whereUserId($user->id)->where('is_deleted',0);

        $this->pagination_query($request, $query);
       return $query->get();
    }

    public function storePet($request){

      return   $request->user()->pets()->create($request->validated());
    }


    public function editPet($request , $pet){

        $data = collect($request->validated())->merge(['created_at' => now()])->toArray();

        $pet->update($data);

        return   $pet;
    }

    public function deletePet($pet){

        $pet->update(['is_deleted' => true]);
    }



    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }

    protected function filterData(){
        return [
            'type'     => new typeScope(),
        ];
    }
}
