<?php


namespace App\Repositories;

use App\Events\phoneChange;
use App\Events\UserLogOut;
use App\Http\Resources\Delivery\DeliveryCheckCode;
use App\Http\Resources\Delivery\DeliveryEditResource;
use App\Http\Resources\Delivery\UserDeliveryResource;
use App\Http\Resources\User\userResource;
use App\Models\Device;
use App\Models\MasterUser;
use App\Models\UploadImage;
use App\Models\VerifyUser;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{

    public  $path;
    public  $token;
    public function __construct( )
    {
        app()->setLocale(request()->headers->get('Accept-Language') ?  : 'ar');

        $this->path = 'files/';
        $this->token = request()->headers->get('Authorization') ? str_replace(' ', '', ltrim(request()->headers->get('Authorization'),'Bearer')) :null;
    }

    public function register($request)
    {

//        $action_code = substr(rand(), 0, 4);
        $action_code = 1111;

        if ($this->token){

            $userInfo = User::whereApiToken($this->token)->first();

            $master_user = MasterUser::whereId($userInfo->master_user_id)->first();
            $master_user->email = $request->email;
            $master_user->password = $request->password;
            $master_user->save();

            $delivery = User::create(['master_user_id' => $master_user->id ,'defined_user' => 'delivery','name' => $request->name ]);

        }else{

          $master_user =   MasterUser::create($request->only(['phone', 'email', 'password']));

            $delivery = $master_user->users()->create([ 'defined_user' => 'delivery','name' => $request->name ]);
            $user    =  $master_user->users()->create([ 'defined_user' => 'user']);
            $this->createVerfiy($request,$user,$action_code);
        }

        $data = $request->only(['id_photo','license','form_picture','car_inside','car_all']);

        $delivery->profile()->updateOrCreate(['user_id' => $delivery->id],$data);

        $this->createVerfiy($request,$delivery,$action_code);

    }


    public function login($request)
    {
        $user =  Auth::guard('master_user')->user()->load('delivery.profile');

        $this->manageDevices($request, $user['delivery']);

        if ($user->delivery->is_accepted == 1){

            return new UserDeliveryResource($user);

        }else{
            return new userResource($user);
        }
    }

    public function forgetPassword($request)
    {
        $delivery = MasterUser::where('phone', $request->phone)->with('delivery')->first();

        $user = $delivery['delivery'];

//        $action_code = substr(rand(), 0, 4);
        $action_code = 1111;

        \event(new phoneChange($user,$action_code,$request));

        return ['code' => $action_code ,'message' => trans('global.activation_code_sent')];
    }

    public function resetPassword($request)
    {
        $user = MasterUser::where('phone', $request->phone)->with('delivery')->first();

        if ($request->password){

            $user->update(['password' => $request->password]);

            $message = trans('global.password_was_edited_successfully');
        }else{

            $message = trans('global.password_not_edited');
        }

        return  [ 'data' => new UserDeliveryResource($user) , 'message' => $message ];
    }

    public function checkCode($request)
    {
        $verifyUser = VerifyUser::where('phone', $request->phone)->with(['user','delivery.master_user'])->get();

        $delivery =  $verifyUser->whereNotNull('delivery')->values()->first();

        $user =  $verifyUser->whereNotNull('user')->values()->first();

        if ($delivery){
            $delivery->delivery->master_user->update([ 'phone' => $request->phone]);
            $delivery->delivery->update([ 'is_active' => 1]);
            $this->manageDevices($request, $delivery->delivery );
        }

        if ($user){
            optional($user->user)->update([ 'is_active' => 1]);
        }

        $verifyUser->each->delete();

        if ($delivery->delivery->is_accepted == 1){

            return new DeliveryCheckCode($delivery['delivery']);
        }else{

            return new userResource($user);
        }
    }

    public function resendCode($request)
    {
        $verifyUser = VerifyUser::where('phone', $request->phone)->with('delivery')->get();

        $user = $verifyUser->whereNotNull('delivery')->values()->first();

//        $action_code = substr(rand(), 0, 4);
        $action_code = 1111;

        \event(new phoneChange($user['delivery'],$action_code,$request));

        return ['code' => $action_code ];
    }

    public function changPassword($request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->master_user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return  ['message' => $message] ;
    }

    public function editProfile($request)
    {
        $user = Auth::user();

        $user->name = $request->name ? $request->name : $user->name;
        $user->image = $request->image ? $request->image : $user->image;

        if ($request->id_photo){
            $user->profile()->update(['id_photo' => $request->id_photo ]);
        }

        if ($request->license){
            $user->profile()->update(['license' =>  $request->license]);
        }
        $user->save();

        $master_user = $user->master_user;
        $master_user->email = $request->email ? $request->email : $master_user->email;
        $master_user->save();

        if ($master_user->phone != $request->phone){
            $action_code = substr(rand(), 0, 4);
            \event(new phoneChange($user,$action_code,$request));
        }

        return new DeliveryEditResource($master_user);
    }

    public function edit_car_delivery($request){
        $user = Auth::user();

        $data = $request->only(['id_photo','license','form_picture','car_inside','car_all']);

        $user->profile()->updateOrCreate(['user_id' => $user->id],$data);

        $master_user = $user->master_user;

        return new DeliveryEditResource($master_user);
    }

    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));
    }

    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            Device::updateOrCreate(['user_id' => $user->id] ,['device' => $request->deviceToken]);

        }
    }

    public function createVerfiy($request , $user , $action_code){
        $verifyPhone = new VerifyUser();
        $verifyPhone->user_id           =    $user->id;
        $verifyPhone->phone             =    $request->phone ? $request->phone : optional($user->master_user)->phone ;
        $verifyPhone->action_code       =    $action_code;
        $verifyPhone->save();
    }


    public function uploadProfileImage($request ){

        $image = UploadImage::where('id',$request->image)->first();

        return $image->image;
    }

    public function uploadImage($request){

        $this->deleteOldImage($request);

        $image = new UploadImage();

        $imageName = $filename = time() . '.' . Str::random(20) . $request->image->getClientOriginalName();
        $request->image->move(public_path('files'), $imageName);

        $image->image = $this->path.$imageName;
        $image->save();

        return [ 'files' => $this->path.$imageName];
    }

    public function removeImage($request){

        $this->deleteOldImage($request);
    }

    function deleteOldImage($request){

        if ($request->image == 1){
            $this->deleteUserImage();
        }

        if ($request->old_files){

            foreach ($request->old_files as $file){
                $last = UploadImage::whereImage($file)->first();

                if ($last){
                    if ($last->image){
                        File::delete(public_path().'/'.$last->image);
                    }

                    $last->delete();
                }
            }
        }
    }


    function deleteUserImage(){
        $user = Auth::user();

        if ($user->image){
            $user->update(['image' => null ]);
        }
    }
}
