<?php


namespace App\Repositories\Interfaces;


interface OrderRepositoryInterface
{


    public function list($request);

    public function show($request);

    public function makeOrder($request);

}
