<?php

namespace App\Models;

use App\User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\Database\HasRolesAndAbilities;

class MasterUser extends Authenticatable
{
    use Notifiable,HasRolesAndAbilities;

    protected $guarded = [ ];

    public function users(){

        return $this->hasMany(User::class,'master_user_id');
    }

    public function default_user(){
        return $this->hasOne(User::class,'master_user_id');
    }

    public function admin(){
        return $this->hasOne(User::class,'master_user_id')->where('defined_user','=','admin');
    }

    public function helper_admin(){
        return $this->hasOne(User::class,'master_user_id')->where('defined_user','=','helper_admin');
    }

    public function user(){
        return $this->hasOne(User::class,'master_user_id')->where('defined_user','=','user');
    }

    public function delivery(){
        return $this->hasOne(User::class,'master_user_id')->where('defined_user','=','delivery');
    }

    public function hasAnyRoles()
    {
        if (auth()->check()) {

            if (auth()->user()->roles->count()) {
                return true;
            }

        } else {
            redirect(route('admin.login'));
        }
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function checkIfHasAbility($name){
        if(auth('master_user')->id() == 1) return true;

        $abilities = auth('master_user')->user()->getAbilities();
        foreach ($abilities as $ab){
            if($ab->name == $name) return true;
        }
        return false;
    }
}
