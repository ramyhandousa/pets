<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

class Profile extends Model
{

    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'car_inside' => 'array',
        'car_all' => 'array',
    ];

    public function getCarInsideAttribute($value)
    {
        $data = json_decode($value);

        if ($data){
            $files = [];
            foreach ($data as $datum){
                $files[] = URL('/'). '/' . $datum;
            }
            return $files;
        }
    }

    public function getCarAllAttribute($value)
    {
        $data = json_decode($value);

        if ($data){
            $files = [];
            foreach ($data as $datum){
                $files[] = URL('/'). '/' . $datum;
            }
            return $files;
        }
    }
}
