<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CulturalVideo extends Model
{

    protected $guarded = [];

    protected $dates = [ "created_at" ];
}
