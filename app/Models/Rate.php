<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table = 'user_rate';

    public $timestamps = false;

    protected $guarded = [];

}
