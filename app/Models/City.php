<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    public function pets(){
        return $this->hasMany(Pet::class,'city_id');
    }

    public function supplies(){

        return $this->hasMany(Supplie::class,'city_id');
    }


}
