<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class OrderOffer extends Model
{

    use  CanBeScoped;

    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function delivery(){
        return $this->belongsTo(User::class,'delivery_id');
    }


}
