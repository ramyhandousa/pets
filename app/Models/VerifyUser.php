<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;


class VerifyUser extends Model
{
       protected $fillable = ['user_id','phone','email' ,'action_code'];


    public function user() {

	    return $this->belongsTo(User::class)->where('defined_user','=','user');
   }

    public function delivery() {

	    return $this->belongsTo(User::class,'user_id')->where('defined_user','=','delivery');
   }

    public static function createOrUpdate($data, $keys)
    {
        $record = self::where($keys)->first();
        if (is_null($record)) {
            return self::create($data);
        } else {
            return self::where($keys)->update($data);
        }
    }
}
