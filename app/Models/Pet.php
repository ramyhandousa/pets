<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
    use  CanBeScoped;
    protected $guarded = [];


    protected $casts = [
        'images' => 'array',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function color(){

        return $this->belongsTo(Color::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }


}
