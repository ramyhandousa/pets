<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use  CanBeScoped;
    protected $guarded = [];


    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(Category::class,'parent_id');
    }

}
