<?php


namespace App\Models\Collections;


use App\User;
use Illuminate\Database\Eloquent\Collection;

class NotificationCustomCollection extends Collection
{

    public function sendToDelivery($title , $body ,$sender_id , $type,$order_id = null){

        $delivery =   $delivery = User::whereDefinedUser('delivery')
                                        ->whereIsAccepted(1)
                                        ->whereIsActive(1)
                                        ->whereIsSuspend(0)->rightJoin('devices', 'users.id', '=', 'devices.user_id')
                                        ->select('user_id','device')->get();

        $devices = $delivery->pluck('device');

//        $ids =   collect($delivery)->unique('user_id')->values()->pluck('user_id');
        $ids =   User::whereDefinedUser('delivery')->whereIsAccepted(1)->whereIsActive(1)->whereIsSuspend(0)->pluck('id');

        $collectionIndex = collect($ids)->map(function ($idDelivery) use ($title , $body , $sender_id , $type,$order_id){
           return [
             'user_id'      => $idDelivery,
             'sender_id'    => $sender_id,
             'order_id'     => $order_id,
             'title'        => $title,
             'body'         => $body,
             'type'         => $type,
           ];
        });

        return ['notifications' => $collectionIndex ,'ids' => $ids , 'devices' =>$devices];
    }


}
