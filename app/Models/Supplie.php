<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Model;

class Supplie extends Model
{
    use  CanBeScoped;
    protected $guarded = [];

    protected $casts = [
      'images' => 'array'
    ];


    public function city(){
        return $this->belongsTo(City::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

}
