<?php

namespace App\Http\Resources\Supplie;

use App\Http\Resources\lists\GeneraterceResource;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplieIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'type'          => $this->definition  ,
            'name'          => $this->name,
            'desc'          => $this->desc,
            'city'          => new GeneraterceResource($this->city),
            'category'      => new GeneraterceResource($this->category),
            'price'         => $this->price,
            'quantity'      => $this->quantity,
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'images'        => $this->images,
        ];
    }
}
