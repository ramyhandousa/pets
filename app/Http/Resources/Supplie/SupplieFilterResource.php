<?php

namespace App\Http\Resources\Supplie;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplieFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->when($this->images, $this->images[0]),
        ];
    }
}
