<?php

namespace App\Http\Resources\lists;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'title'         => $this->title  ,
            'desc'          => $this->when($this->desc,  $this->desc),
            'image'         => $this->when($this->image,  $this->image),
            'link_video'    => $this->when($this->url,  $this->url),
            'video'         => $this->when($this->file,  $this->file),
        ];
    }
}
