<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderListGuestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->invitation_id,
            'name'          => $this->invitation->name,
            'image'         => $this->when($this->invitation->image,URL('/').'/'. $this->invitation->image),
            'city'          => $this->invitation->city_id,
            'address'       => $this->invitation->address,
            'created_at'    => $this->getArabicMonth( $this->invitation->created_at),
            'status'        => $this->when($this->status, $this->status),
        ];
    }
    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        $data = ['day' => $day , 'month' => $month];
        return $data;
    }
}
