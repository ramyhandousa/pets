<?php

namespace App\Http\Resources\Orders;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'image'         => $this->when($this->image,URL('/').'/'. $this->image),
            'city'          => $this->city_id,
            'address'       => $this->address,
            'created_at'    => $this->getArabicMonth( $this->created_at),
        ];
    }


    function getArabicMonth($data) {

        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];

        $day = date("d", strtotime($data));
        $month = date("M", strtotime($data));
        $year = date("Y", strtotime($data));

        $month = $months[$month];

        $data = ['day' => $day , 'month' => $month];
        return $data;
    }
}
