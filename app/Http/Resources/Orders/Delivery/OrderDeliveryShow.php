<?php

namespace App\Http\Resources\Orders\Delivery;

use App\Http\Resources\Orders\Users\OrderDetailsResource;
use App\Http\Resources\Pets\PetFilterResource;
use App\Http\Resources\Supplie\SupplieResource;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class OrderDeliveryShow extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $have_price_offer =  $this->status == 'new' && $this->delivery_offer(Auth::id()) ;

        $deliveryId = $this->delivery_id ;
        return [
            'id'                  => $this->id,
            'status'              => $have_price_offer ? "pending" :$this->status,
            'total_price'         =>   $this->total_price + $this->delivery_price,
            'delivery_price'      => $this->when( $this->delivery_offer($deliveryId ? $deliveryId : Auth::id() ),$this->delivery_offer($deliveryId ? $deliveryId : Auth::id() )) ,
            $this->mergeWhen($this->status != 'new' && $this->status != 'pending',[
                'client_phone'    =>  optional($this->user)->master_user->phone ,
                'pet_phone'       =>  optional( optional( optional($this->pet)->user)->master_user)->phone ,
                'price'           =>  $this->price ,
                'delivery_price'  =>  $this->delivery_price ,
                'rate'            =>  $this->when(optional($this->rate_order)->rate,optional($this->rate_order)->rate)
            ]),
//            'distance_from'       => $this->type == 'pets' ? new LocationIndex($this->pet) : new LocationIndex($this->supplie),
            'distance_to'         => new LocationIndex($this),

            'message'             => $this->when($this->message, $this->message),

            'order_details' => OrderDetailsResource::collection($this->order_details)

        ];
    }
}
