<?php

namespace App\Http\Resources\Orders\Delivery;

use App\Http\Resources\Orders\Users\OrderDetailsFilter;
use App\Http\Resources\Pets\PetsIndexResource;
use App\Models\Pet;
use App\Models\Supplie;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDeliveryIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $filter = $request->filter;
        return [
            'id'            => $this->id,
            'status'        => $filter == 'pending' ? 'pending' : $this->status,
            'distance_to'   =>  new LocationIndex($this),
            'created_at'    => $this->created_at,
            'order_details' => new  OrderDetailsFilter($this->order_details->first())
        ];
    }
}
