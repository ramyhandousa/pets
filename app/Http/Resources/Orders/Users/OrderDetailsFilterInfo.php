<?php

namespace App\Http\Resources\Orders\Users;

use App\Http\Resources\Orders\Delivery\LocationIndex;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsFilterInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'type'          => $this->definition,
            'name'          => $this->name,
            'image'        => $this->images[0],
            'origin'              =>  new LocationIndex($this)
        ];
    }
}
