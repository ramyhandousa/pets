<?php

namespace App\Http\Resources\Orders\Users;

use App\Http\Resources\Pets\PetsResource;
use App\Http\Resources\Supplie\SupplieIndexResource;
use App\Models\Pet;
use App\Models\Supplie;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $check_if_pet =  ($this->type == "animal" || $this->type == "plant")  && $this->is_supplie == 0 ;
        return [
            "quantity"  =>  $this->quantity,
            "price"     => $this->price,
            "is_supply"     => $check_if_pet != true,
            "info"      => $check_if_pet ?
                            new OrderDetailsFilterInfo(Pet::find($this->general_id)) :
                            new OrderDetailsFilterInfo(Supplie::find($this->general_id))
        ];
    }
}
