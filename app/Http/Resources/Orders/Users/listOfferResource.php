<?php

namespace App\Http\Resources\Orders\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class listOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $delivery = $this->delivery;
        return [
            'id'        => $this->id,
            'price'     => $this->delivery_price,
            'name'      => $this->when($delivery->name,$delivery->name),
            'phone'      => $this->when(optional($delivery->master_user)->phone,optional($delivery->master_user)->phone),
            'image'     => $this->when($delivery->image , $delivery->image),
            'rate'      => $delivery->rate_delivery_count(),
        ];
    }
}
