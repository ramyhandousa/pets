<?php

namespace App\Http\Resources\Orders\Users;

use App\Http\Resources\Delivery\DeliveryFilterResource;
use App\Http\Resources\Pets\PetFilterResource;
use App\Http\Resources\Pets\PetsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderPetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'type'          => $this->type,
            'price'         => $this->price,
            'total_price'   =>   ($this->price  *  $this->quantity)  + $this->delivery_price,
            'quantity'      => $this->quantity ,
            'status'        => $this->status,
            'message'       => $this->when($this->message,$this->message),
            'created_at'    =>  $this->created_at,
            'rate'            =>  $this->when(optional($this->rate_order)->rate,optional($this->rate_order)->rate),
            $this->mergeWhen(in_array($this->status ,['new', 'pending']),[
                'offer_count'   => $this->offer_price_count(),
            ]),
            $this->mergeWhen($this->status != 'new'&& $this->status != 'pending' && $this->delivery,[
                'delivery'   => new DeliveryFilterResource($this->delivery),
                'delivery_price' => $this->when($this->delivery_price , $this->delivery_price),
            ]),
            'info'           => new PetFilterResource($this->pet),
        ];
    }
}
