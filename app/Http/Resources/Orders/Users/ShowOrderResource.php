<?php

namespace App\Http\Resources\Orders\Users;

use App\Http\Resources\Delivery\DeliveryFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ShowOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'total_price'   =>   $this->total_price + $this->delivery_price,
            'status'        => $this->status,
            'message'       => $this->when($this->message,$this->message),
            'created_at'    =>  $this->created_at,
            'rate'            =>  $this->when(optional($this->rate_order)->rate,optional($this->rate_order)->rate),
            $this->mergeWhen(in_array($this->status ,['new', 'pending']),[
                'offer_count'   => $this->offer_price_count(),
            ]),
            $this->mergeWhen($this->status != 'new'&& $this->status != 'pending' && $this->delivery,[
                'delivery'   => new DeliveryFilterResource($this->delivery),
                'delivery_price' => $this->when($this->delivery_price , $this->delivery_price),
            ]),

            'order_details' => OrderDetailsResource::collection($this->order_details)
        ];
    }
}
