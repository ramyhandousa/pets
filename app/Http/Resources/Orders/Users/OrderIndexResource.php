<?php

namespace App\Http\Resources\Orders\Users;

use App\Http\Resources\Pets\PetsIndexResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderIndexResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'total_price'   =>   $this->total_price   ,
            'created_at'    =>  $this->created_at,
            'status'        => $this->status,
            $this->mergeWhen(in_array($this->status ,['new', 'pending']),[
                'offer_count'   => $this->offer_price_count(),
            ]),

            'order_details' => new  OrderDetailsFilter($this->order_details->first())
        ];
    }


//    function getArabicMonth($data) {
//
//        $months = [ "Jan" => "يناير", "Feb" => "فبراير", "Mar" => "مارس", "Apr" => "أبريل",
//            "May" => "مايو", "Jun" => "يونيو", "Jul" => "يوليو", "Aug" => "أغسطس",
//            "Sep" => "سبتمبر", "Oct" => "أكتوبر", "Nov" => "نوفمبر", "Dec" => "ديسمبر" ];
//
//        $day = date("d", strtotime($data));
//        $month = date("M", strtotime($data));
//        $year = date("Y", strtotime($data));
//
//        $month = $months[$month];
//
//        return $day . ' ' . $month ;
//    }
}
