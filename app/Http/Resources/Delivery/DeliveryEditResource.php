<?php

namespace App\Http\Resources\Delivery;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryEditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->delivery->id,
            'master_id'     => $this->id,
            'defined_user'  => $this->delivery->defined_user,
            'name'          => $this->delivery->name,
            'phone'         => $this->phone,
            'email'         => $this->when($this->email , $this->email),
            'image'         => $this->when($this->delivery->image , URL('/').'/'. $this->delivery->image),
            'api_token'     => $this->delivery->api_token ,
            'is_active'     => $this->delivery->is_active ? 1  == true : false,
            'is_accepted'   => $this->delivery->is_accepted ? 1  == true : false,
            'is_suspend'    => $this->delivery->is_suspend ? 1  == true : false,
            'message'       => $this->when($this->delivery->message , $this->delivery->message),
            $this->mergeWhen( $request->phone && $request->user()->master_user->phone !== $request->phone && $request->path() ==  'api/v1/Delivery/Auth/editProfile',[
                'code'          => $this->when($this->delivery->code, optional($this->delivery->code)->action_code),
            ]),
            $this->mergeWhen( $request->has('profile'),[
                'profile'       => new ProfileDeliveryResource($this->delivery->profile)
            ])
        ];
    }
}
