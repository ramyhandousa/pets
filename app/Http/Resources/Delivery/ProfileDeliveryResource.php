<?php

namespace App\Http\Resources\Delivery;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfileDeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_photo'      => URL('/') . '/'. $this->id_photo,
            'license'       => URL('/') . '/'. $this->license,
            'form_picture'  => URL('/') . '/'. $this->form_picture,
            'car_inside'    => $this->car_inside,
            'car_all'       => $this->car_all,
        ];
    }
}
