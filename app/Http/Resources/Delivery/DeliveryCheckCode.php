<?php

namespace App\Http\Resources\Delivery;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryCheckCode extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'master_id'     => $this->master_user->id,
            'defined_user'  => $this->defined_user,
            'name'          => $this->name,
            'phone'         => $this->master_user->phone,
            'email'         => $this->when($this->master_user->email , $this->master_user->email),
            'image'         => $this->when($this->image ,  URL('/').'/'. $this->image),
            'api_token'     => $this->api_token ,
            'is_active'     => $this->is_active ? 1  == true : false,
            'is_accepted'   => $this->is_accepted ? 1  == true : false,
            'is_suspend'    => $this->is_suspend ? 1  == true : false,
            'message'       => $this->when($this->message , $this->message),
            $this->mergeWhen( $request->has('profile'),[
                'profile'       => new ProfileDeliveryResource($this->profile)
            ])
        ];
    }
}
