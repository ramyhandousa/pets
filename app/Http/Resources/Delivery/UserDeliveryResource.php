<?php

namespace App\Http\Resources\Delivery;

use Illuminate\Http\Resources\Json\JsonResource;

class UserDeliveryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->delivery->id,
            'master_id'     => $this->id,
            'defined_user'  => $this->delivery->defined_user,
            'name'          => $this->delivery->name,
            'phone'         => $this->phone,
            'email'         => $this->when($this->email , $this->email),
            'image'         => $this->when($this->delivery->image ,  URL('/').'/'. $this->delivery->image),
            'api_token'     => $this->delivery->api_token ,
            'is_active'     => $this->delivery->is_active ? 1  == true : false,
            'is_accepted'   => $this->delivery->is_accepted ? 1  == true : false,
            'is_suspend'    => $this->delivery->is_suspend ? 1  == true : false,
            'bank_name'       => $this->when($this->delivery->bank_name , $this->delivery->bank_name),
            'iban'            => $this->when($this->delivery->iban , $this->delivery->iban),
            'account_number'  => $this->when($this->delivery->account_number , $this->delivery->account_number),
            'message'       => $this->when($this->delivery->message , $this->delivery->message),
            $this->mergeWhen( $request->has('profile'),[
                'profile'       => new ProfileDeliveryResource($this->delivery->profile)
            ])


        ];
    }
}
