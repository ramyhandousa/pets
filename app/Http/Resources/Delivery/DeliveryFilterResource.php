<?php

namespace App\Http\Resources\Delivery;

use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'name'  => $this->when($this->name,$this->name),
            'image' => $this->when($this->image ,  URL('/').'/'. $this->image),
            'phone' => $this->master_user->phone,
            'rate'  => $this->rate_delivery_count(),
        ];
    }
}
