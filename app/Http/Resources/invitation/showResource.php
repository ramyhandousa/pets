<?php

namespace App\Http\Resources\invitation;

use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;

class showResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = app('translator')->getLocale();
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'image'             => $this->when( $this->image, URL('/').'/'.$this->image),
            'date'              => $this->date,
            'time_from'         => $this->time_from,
            'time_to'           => $this->time_to,
            'number_guests'     => $this->number_guests,
            'desc'              => $this->desc,
            'latitude'          => $this->latitude,
            'longitude'         => $this->longitude,
            'address'           => $this->address,
            'city'              => $lang == 'ar' ? optional($this->city)->name_ar : optional($this->city)->name_en ,
            'is_payed'          => (boolean) $this->is_payed,
            'payment_system'    => Setting::getBody('payment_value') * $this->number_guests,
            'payment'           => $this->payment,
            'payment_money'     => $this->payment_money,
            'count_guest'       => $this->guest_contact->count() > 7,
            'count_supervisor'  => $this->supervisor_contact->count() > 2,
            'guest'             => filterRelation::collection($this->guest_contact->take(7)),
            'supervisor'        => filterRelation::collection($this->supervisor_contact->take(2)),
        ];
    }
}
