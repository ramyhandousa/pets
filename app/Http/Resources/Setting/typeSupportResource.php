<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\JsonResource;

class typeSupportResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' =>   $this->name_ar   ,
        ];
    }
}
