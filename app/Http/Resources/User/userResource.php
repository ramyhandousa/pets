<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class userResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->user->id,
            'master_id'     => $this->id,
            'defined_user'  => $this->user->defined_user,
            'name'          => $this->when($this->user->name , $this->user->name),
            'phone'         => $this->phone,
            'email'         => $this->when($this->email , $this->email),
            'image'         => $this->when($this->user->image ,  URL('/').'/'. $this->user->image),
            'api_token'     => $this->user->api_token ,
            'is_active'     => $this->user->is_active ? 1  == true : false,
            'is_suspend'    => $this->user->is_suspend ? 1  == true : false,
            'bank_name'       => $this->when($this->user->bank_name , $this->user->bank_name),
            'iban'            => $this->when($this->user->iban , $this->user->iban),
            'account_number'  => $this->when($this->user->account_number , $this->user->account_number),
            'message'           => $this->when($this->user->message , $this->user->message),
        ];
    }
}
