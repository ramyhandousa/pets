<?php

namespace App\Http\Resources\Pets;

use App\Http\Resources\lists\GeneraterceResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PetFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'gender'          => $this->type,
            'name'          => $this->name,
            'desc'          => $this->desc,
            'city'          => new GeneraterceResource($this->city),
            'category'      => new GeneraterceResource($this->category),
            'color'         => new GeneraterceResource($this->color),
            'price'         => $this->price,
            'age'           => $this->age,
            'quantity'      => $this->quantity,
            'weight'        => $this->when($this->weight,$this->weight),
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'image'        => $this->images[0],
        ];
    }
}
