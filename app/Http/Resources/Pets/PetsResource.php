<?php

namespace App\Http\Resources\Pets;

use App\Http\Resources\lists\GeneraterceResource;
use Illuminate\Http\Resources\Json\JsonResource;

class PetsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'type'          => $this->definition  ,
            'user_id'       => $this->user_id,
            'gender'        => $this->type,
            'name'          => $this->name,
            'desc'          => $this->desc,
            'city'          => new GeneraterceResource($this->city),
            'category'      => new GeneraterceResource($this->category),
            'color'         => new GeneraterceResource($this->color),
            'price'         => $this->price,
            'age'           => $this->when($this->age,$this->age),
            'quantity'      => $this->quantity,
            'weight'        => $this->when($this->weight,$this->weight),
            'latitude'      => $this->latitude,
            'longitude'     => $this->longitude,
            'address'       => $this->address,
            'images'        => $this->images,
            'video'         => $this->when($this->video,$this->video),
        ];
    }
}
