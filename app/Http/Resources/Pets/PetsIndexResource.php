<?php

namespace App\Http\Resources\Pets;

use Illuminate\Http\Resources\Json\JsonResource;

class PetsIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'price'     => $this->price,
            'city_name' => $this->city->name_ar,
            'image'     => $this->when($this->images, $this->images[0]),
            'video'     => $this->video,
        ];
    }
}
