<?php

namespace App\Http\Requests\Admin;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class storeSupplies extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'definition'    => 'required',
            'name'          => 'required',
            'desc'          => 'required',
            'city_id'       => 'required',
            'category_id'   => 'required',
            'quantity'      => 'required|max:8',
            'latitude'      => 'required',
            'longitude'     => 'required',
            'address'       => 'required',
            'price'         => 'required|max:8',
            'images.*'      => 'image|mimes:jpeg,png,jpg|max:10240',
        ];
    }

    public function messages()
    {
        return [
            'images.image' => 'تاكد من صورة   المستلزم صورة فقط ',
            'images.mimes' => '    تأكد من صورة   المستلزم ان لا تكون سوي  jpeg,png,jpg ',
            'images.*.image' => 'تاكد من انك تضيف صور فقط ',
            'images.*.mimes' => '    تأكد من ان الصور المرفقة لا تكون سوي  jpeg,png,jpg ',
            'quantity.max' => '  اكبر كمية ممكنه هي 8 ارقام فقط ',
            'price.max' => '  اكبر كمية ممكنه هي 8 ارقام فقط ',
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

           $images = $this->images;

           if (!$images){
               $validator->errors()->add('files', 'يجب اختيار  جميع الصور للرفع  ');

           }

           if ($images){
               if (    count($this->images) < 4 ){

                   $validator->errors()->add('files', 'يجب اختيار  4 صور من فضلك  ');
               }
           }


        });
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->first();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 422));
    }
}
