<?php

namespace App\Http\Requests\Admin;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class VaildVideoUpload extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'title'  => "required|max:255",
           'desc'   => "required",
           'url'    => 'nullable|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
           'file'   => "mimes:flv,mp4,m3u8,ts,3gp,mov,avi,wmv|max:20000",
            'image' => 'nullable|image'
        ];
    }


    public function messages()
    {
        return [
            'url.regex' => 'تاكد من صيغة اللينك المرفوع من فضلك ',
            'file.mimes' => '    تأكد من الفيديو لا يكون سوي  flv,mp4,m3u8,ts,3gp, mov, avi, wmv ',
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 422));
    }
}
