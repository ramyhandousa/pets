<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class sendPromoCodeVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $vaild = in_array($this->type , [ "user" , "delivery" , "agent" ]);

        return [
            'users' => Rule::requiredIf($vaild),
        ];
    }

    public function messages()
    {
        return [
            "users.required" => "من فضلك إختار الأشخاص المستهدفة لإرسال الإشعار"
        ];
    }
}
