<?php

namespace App\Http\Requests\api\Order\Delivery;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class makeOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order =  $this->route('order');

        $offer = $order->my_delivery_offer();

        if ($offer){
            if ($offer->delivery_id == Auth::id()){

                return true;
            }else{
                return false;
            }
        }
           return  true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delivery_price' => 'required|numeric'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order =  $this->route('order');

            $offer = $order->my_delivery_offer();

            if ( !$offer ) {
                $validator->errors()->add('unavailable',  ' تأكد من أن لديك طلب لتقديم عرض     ');
                return ;
            }

            $price = $offer->delivery_price;


            if ( $price !== null ) {
                $validator->errors()->add('unavailable', $price. ' تم تقديم عرض مسبقا بقيمة ');
            }

        });
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
