<?php

namespace App\Http\Requests\api\Order\Delivery;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class orderFinish extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order =  $this->route('order');

        if ($order->delivery_id == Auth::id()){

            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            $current = $order['status'] === 'current';

            if ( !$current ) {
                $validator->errors()->add('unavailable', '  تأكد من فضلك بان طلبك في الجاري لتاكيد التوصيل ');
                return;
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
