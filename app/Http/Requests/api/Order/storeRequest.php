<?php

namespace App\Http\Requests\api\Order;

use App\Models\Pet;
use App\Models\Supplie;
use App\Rules\checkPromoCode;
use App\Rules\checkQunatity;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class storeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'type'          => 'required|in:pets,supplies',
//            'pet_id'        => 'required_if:type,===,pets|exists:pets,id',
//            'supplie_id'    => 'required_if:type,===,supplies|exists:supplies,id',
//            'quantity'      => ['required' ,'numeric','min:1','max:100000',new checkQunatity($this->type , $this->pet_id, $this->supplie_id)],
            'latitude'                  => 'required',
            'longitude'                 => 'required',
            'address'                   => 'required',
            'promo_code'                =>  ['nullable','max:50','exists:promo_codes,code' ,new checkPromoCode()],
            'discount_code'             =>  'max:50',
            'discount_value'            =>  'max:50',

            'order_details'                  =>  'required|array',
            'order_details.*.type'           =>  'required|in:animal,plant',
            'order_details.*.general_id'     =>  ['required' , 'distinct'],

            'order_details.*.price'          =>  'required',
            'order_details.*.is_supplie'     =>  'required|boolean',
            'order_details.*.quantity'       =>  'required|max:255',
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $delivery = User::whereDefinedUser('delivery')->whereIsActive(1)
                ->whereIsSuspend(0)->exists();

            if (! $delivery ) {
                $validator->errors()->add('unavailable', 'للأسف لا يوجد مندوبين حاليا لإستقبال طلبك');
            }

            $pets_ids = [];
            $plant_ids = [];
            $quantity = [];


            foreach ($this->order_details as $ket =>  $detail){

                if ($detail['type'] === 'animal' && $detail['is_supplie'] == 0){

                    $pets_ids[] = $detail['general_id'];

                    $pet = Pet::whereId($detail['general_id'])->first();

                    if ($pet){
                        if ($detail['quantity'] > $pet->quantity){
                            if ($pet->quantity > 0){

                                $quantity[$ket] = $pet->quantity." اقصي كمية من الحيوان " .  $pet->name. " هي ";
                            }else{

                                $quantity[$ket] =  " للاسف لا يوجد كمية من الحيوان " . $pet->name;
                            }
                        }
                    }

                }else{

                    $plant_ids[] = $detail['general_id'];

                    $supplie = Supplie::whereId($detail['general_id'])->first();

                    if ($supplie){
                        if ($detail['quantity'] > $supplie->quantity){
                            if ($supplie->quantity > 0){

                                $quantity[$ket] =  $supplie->quantity. "  اقصي كمية من المستلزم ". $supplie->name ." هي ";
                            }else{

                                $quantity[$ket] =  " للاسف لا يوجد كمية من المستلزم " . $supplie->name;
                            }
                        }
                    }

                }
            }

            if (count($pets_ids) > 0) {

                $count_pets = Pet::whereIn('id', $pets_ids)->count();


                if (count($pets_ids) !== $count_pets ) {
                    $validator->errors()->add('unavailable', 'أتاكد من فضلك ان الحيوانات الي يتم إضافتها متاحة');
                    return;
                }
            }

            if (count($plant_ids) > 0) {
                $supplie_count = Supplie::whereIn('id', $plant_ids)->count();

                if (count($plant_ids) !== $supplie_count ) {
                    $validator->errors()->add('unavailable', 'أتاكد من فضلك ان المستلزمات الي يتم إضافتها متاحة');
                    return;
                }
            }

            if (count($quantity) > 0) {

                $message = collect($quantity)->first();

                if ($message ) {
                    $validator->errors()->add('unavailable', $message);
                    return;
                }
            }


        });
    }

    public function validated()
    {
        $data =  parent::validated(); // TODO: Change the autogenerated stub

//        return $data;
        return collect($data)->except('order_details')->toArray();
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}
