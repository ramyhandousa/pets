<?php

namespace App\Http\Requests\api\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class listOfferUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order =  $this->route('order');


        if ($order->user_id == Auth::id()){

            return true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            $exist = $order['status'] !== 'pending';

            if ( $exist ) {
                $validator->errors()->add('unavailable', 'للأسف طلبك غير معلق  تأكد من فضلك ');
                return ;
            }

            if ( $order->time_out == 0 ) {
                $validator->errors()->add('time_out', ' لم يتم إنتهاء الوقت لمشاهدة العروض ');
                return ;
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
