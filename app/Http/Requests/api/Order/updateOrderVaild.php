<?php

namespace App\Http\Requests\api\Order;

use App\Models\Order;
use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class updateOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            $exist = $order->whereStatus('empty')->exists();

            if (! $exist ) {
                $validator->errors()->add('unavailable', 'للأسف طلبك غير معلق تأكد من فضلك ');
            }

            $designer = User::whereDefinedUser('designer')->whereIsAccepted(1)->whereIsActive(1)
                ->whereIsSuspend(0)->whereHas('profile')->exists();

            if (! $designer ) {
                $validator->errors()->add('unavailable', 'للأسف لا يوجد مهندسين حاليا لإستقبال طلبك');
            }
        });
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
