<?php

namespace App\Http\Requests\api\offer;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class vaildListOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules(){
        return [];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            $time_out = $order->time_out;

            if ( $time_out === 0 ) {
                $validator->errors()->add('unavailable', 'للأسف لم يتم الإنتهاء من الوقت لظهور العروض المقدمة لديك ');
            }


        });
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
