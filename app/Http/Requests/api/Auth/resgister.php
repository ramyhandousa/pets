<?php

namespace App\Http\Requests\api\Auth;

use App\Models\MasterUser;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class resgister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }



    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $token = str_replace(' ', '', ltrim(request()->headers->get('Authorization'),'Bearer'));

        $user = User::whereApiToken($token)->first();

        $id = $user ? $user->master_user_id  :null;

        return [
            'name' => 'required|max:75',
            'phone'         => 'numeric|digits_between:9,15|unique:master_users,phone,' .$id,
            'email'         => 'email|unique:master_users,email,' .$id ,
            'password' => 'required|min:6',
            'id_photo' => 'required',
            'license' => 'required',
            'form_picture' => 'required',
            'car_inside' => 'required',
            'car_all' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => trans('validation.required'),
            'email.required' => trans('validation.required'),
            'email.unique' => trans('global.unique_email'),
            'phone.unique' => trans('global.unique_phone'),
            'phone.required' => trans('validation.required'),
            'password.required' => trans('validation.required'),
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $master_user = MasterUser::wherePhone($this->phone)->with('users')->first();

            if ($master_user){
               $delivery =  $master_user->users->where('defined_user','delivery')->where('is_accepted',0)->first();

                if ($delivery ) {
                    $validator->errors()->add('unavailable', 'لديك طلب إنضمام سابق برجاء الإنتظار .');
                }

               $delivery_accepted =  $master_user->users->where('defined_user','delivery')->where('is_accepted',1)->first();

                if ($delivery_accepted ) {
                    $validator->errors()->add('unavailable', '  تم إنضامك كمندوب بنجاح مسبقا .. ');
                }
            }

        });
    }


    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }


}
