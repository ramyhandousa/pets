<?php

namespace App\Http\Requests\api\Auth;

use App\Models\MasterUser;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'phone' => 'required',
            'password' => 'required',
            'deviceType' => 'in:android,Ios',
        ];
    }

   public function messages()
   {
    return [
      'phone.required' => trans('global.required'),
      'password.required' => trans('global.required'),
      'deviceType.in' => 'android او Ios  يجب ان يكون بين الكلمتين ',
    ];
   }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){


            if (! $token = Auth::guard('master_user')->attempt(['phone' =>  $this->phone, 'password' => $this->password])) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }

            $user = Auth::guard('master_user')->user();

            if ($user){

                $delivery = Auth::guard('master_user')->user()->load('delivery');

                if (!$delivery['delivery']){
                    $validator->errors()->add('unavailable', 'للأسف ليس لديك اكونت مندوب لدينا');
                }
            }

        });
    }

      protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
