<?php

namespace App\Http\Requests\api\Auth;

use App\Rules\checkOldPass;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class editUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user()->master_user;

        return [
            'name'          => 'max:255',
            'phone'         => 'numeric|digits_between:9,15|unique:master_users,phone,' . $user->id ,
            'email'         => 'email|unique:master_users,email,' . $user->id ,
//            'image'         => 'mimes:jpeg,png,jpg|max:10240'
        ];
    }


    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'email.email' => trans('validation.email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
