<?php

namespace App\Http\Requests\api\Pet;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class editVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = Auth::user();
        if ($user->is_active == 1 && $user->is_suspend == 0){
            return true;
        }else{
            return false;
        }
    }

    public function rules()
    {
        return [
            'type'          => 'in:male,female',
            'name'          =>  'max:75',
            'desc'          => 'min:3|max:20000',
            'city_id'       => 'exists:cities,id',
            'category_id'   => 'exists:categories,id',
            'color_id'      => 'exists:colors,id',
            'price'         => 'numeric|min:1|max:100000',
            'age'           => 'numeric|min:1',
            'quantity'      => 'numeric|min:1',
            'weight'        => '',
            'latitude'      => '',
            'longitude'     => '',
            'address'       => '',
            'images'        => 'array',
            'video'         => ''
        ];
    }


    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
