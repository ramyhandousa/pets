<?php

namespace App\Http\Requests\api\payment;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class payOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        $offer = $this->route('orderOffer');
//        $order = $offer->order;
//
//        if ($order->user_id == Auth::id()){
//            return true;
//        }else{
//            return false;
//        }

        return  true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'paymentId' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'paymentId.required' => 'من فضلك ادخل الرقم الخاص بعميلة الدفع',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $offer = $this->route('orderOffer');

            $order = $offer->order;

            if ($offer){ // Need Offer for first time pay order

                if ($order->is_pay == 1){ // Valid Transfer Many Times
                    $validator->errors()->add('unavailable', ' برجاء التأكد لانه تم الدفع مسبقا ...  ');
                }
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
