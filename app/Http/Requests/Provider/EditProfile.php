<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class EditProfile extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();
        return [
            'phone' => 'required|regex:/(05)/|min:10|max:10|unique:users,phone,'. $user->id ,
            'email' => 'required|unique:users,email,'. $user->id ,
            'image' => 'image|mimes:jpeg,png,jpg|max:1000'
        ];
    }


    public function messages()
    {
        return [
            'phone.required' => 'رقم الهاتف مطلوب',
            'phone.unique' => 'هذا الرقم مستخدم من قبل',
            'email.required' => 'البريد الإلكتروني مطلوب',
            'email.unique' => 'هذا البريد مستخدم من قبل',
            'image.max' => '  اقصي حجم مسموح به هو 1 ميجا للصورة   ',
            'phone.regex' => 'يجب ادخال صيغة هاتف صحيحة ليبدا من (05) ',

        ];
    }
}
