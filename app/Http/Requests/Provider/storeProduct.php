<?php

namespace App\Http\Requests\Provider;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
class storeProduct extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'image.*' => 'image|mimes:jpeg,png,jpg|max:1000'
        ];
    }

    public function messages()
    {
        return [
            'image.*.image' => ' يجب ان يكون صور  المنتج عبارة عن صورة ',
            'image.*.max' => '  اقصي حجم مسموح به هو 1 ميجا للصورة   ',
            'image.*.mimes' => 'يجب إضافة صورة لإضافة مدير وتكون بصيغة PNG او JPG',
        ];
    }

    public function withValidator($validator)
    {

        $validator->after(function ($validator) {

            if (    count($this->oldImage) == 0 ){

                $validator->errors()->add('files', 'يجب اختيار صورة واحدة علي الأقل ');
            }

        });
        return;
    }

    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->first();

        throw new HttpResponseException(response()->json([ 'message' => $values], 400));
    }

}
