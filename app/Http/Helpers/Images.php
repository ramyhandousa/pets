<?php

namespace App\Http\Helpers;

use Illuminate\Support\Str;

use Image;

class Images
{

    public static function uploadMainImage($request, $name, $path = null)
    {

        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . $image->getClientOriginalName();

            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }

    /**
     * RETURN path to save in images tables DATABASE
     * @RETURN IMAGE PATH
     *
     * SAVE THUMBNAILS IMAGES
     */
    public static function uploadImage($request, $name, $path = null, $width = null, $height = null)
    {
        if ($request->hasFile($name)):
            // Get File name from POST Form
            $image = $request->file($name);

            // Custom file name with adding Timestamp
            $filename = time() . '.' . Str::random(20) . $image->getClientOriginalName();

            // Directory Path Save Images
            $path = public_path($path . $filename);

            // Upload images to Target folder By INTERVENTION/IMAGE
            $img = Image::make($image);

            // RESIZE IMAGE TO CREATE THUMBNAILS
            if (isset($width) || isset($height))
                $img->resize($width, $height, function ($ratio) {
                    $ratio->aspectRatio();
                });
            $img->save($path);

            // RETURN path to save in images tables DATABASE
            return $filename;
        endif;
    }

    public static function uploadVideo($request , $name, $pathInPublic){

        if ($request->hasFile($name)):

            $myUpload = explode('.', $_FILES[$name]["name"]);
//            $fileName = current($myUpload);
            $extension = end($myUpload);

            $file = $request->file($name);
            $filename = trim(time() . '.' . $extension);
            // $filename = trim(time() . '.' . str_slug($file->getClientOriginalName()));
            $path = public_path().$pathInPublic ;
            $file->move($path, $filename);


            return $filename;

        endif;
    }


    public function getDefaultImage($image, $defaultImagePath)
    {
        return ($image != '') ? $image : $defaultImagePath;
    }

}
