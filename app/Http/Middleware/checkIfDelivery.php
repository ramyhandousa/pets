<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class checkIfDelivery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = str_replace(' ', '', ltrim(request()->headers->get('Authorization'),'Bearer'));

        if ( $request->header('Authorization')){

            $user = User::whereApiToken($token)->first();

            if ($user->defined_user !== 'delivery'){
                return response()->json([   'status' => 401,  'error' => (array) 'تأكد من أنك مندوب من فضلك'   ],200);
            }
        }
        return $next($request);
    }
}
