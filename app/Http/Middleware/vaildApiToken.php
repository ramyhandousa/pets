<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class vaildApiToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = str_replace(' ', '', ltrim(request()->headers->get('Authorization'),'Bearer'));

        if ( $request->header('Authorization')){

            $user = User::whereApiToken($token)->first();

            if (!$user){
                return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
            }
        }
        return $next($request);
    }
}
