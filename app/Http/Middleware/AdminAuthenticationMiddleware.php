<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Auth;


class AdminAuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

//        if (auth()->check() && auth()->user()->hasAnyRoles() ) {
        if (Auth::guard('master_user')->check()  ) {

            return $next($request);
        }else {

            return redirect(route('admin.login'));
        }
    }
}
