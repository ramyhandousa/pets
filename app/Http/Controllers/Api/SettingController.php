<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\message\storeVaild;
use App\Http\Requests\api\setting\bankTransferRequest;
use App\Http\Requests\api\setting\contactVaild;
use App\Http\Requests\api\setting\ReportVaild;
use App\Http\Resources\Setting\BankResource;
use App\Http\Resources\Setting\typeSupportResource;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\Device;
use App\Models\Message;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypeSupport;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public $push;
    public  $notify;
    use RespondsWithHttpStatus;
    public function __construct(InsertNotification $notification , PushNotification $push )
    {
        $this->middleware('auth:api')->only(['bank_transfer','contact_us','report','testingNotification']);
        $this->push = $push;
        $this->notify = $notification;
    }

    public function updateTimeOut(Order  $order){

        $order->load('user.devices');

        $notify = $this->notify->NotificationDbType(8,$order->user_id,null, null,$order);

        foreach ($order['user']['devices'] as $device):

            $this->push->sendPushNotification((array) $device['device'], null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'orderId'       => $notify['order_id'],
                    'type'          => $notify['type'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'created_at'    => $notify['created_at'],
                ]
            );

        endforeach;
        $order->update(['time_out' => 1]);
        return true;
    }

    public function testingNotification(storeVaild $request){

        $user = Auth::user();
        $data =  $request->validated();

        $data['user_id']  = $user->id;

        $message = Message::create($data);

        $devices = Device::where('user_id', 45)->pluck('device');
       return $this->push->sendPushNotification($devices, null, $request->title? : 'المصمم الذكي',$request->message? : 'testing' ,
            [
                'id'                => $request->id,
                'user_id'           =>  $user->id ,
                'user_image'        =>  $request->user_image ,
                'conversation_id'   =>  $request->conversation_id ,
                'message'           =>  $request->message ,
                'orderId'           =>  $request->orderId ,
            ]
        );
    }

    public function banks(){

        $banks = Bank::all();

        $data = BankResource::collection($banks);

        return $this->success('الحسابات البنكية',$data);
    }


    public function bank_transfer(bankTransferRequest $request){

        $user = Auth::user();
        $bankTransfer = new BankTransfer();
        $bankTransfer->user_id = $user->id;
        $bankTransfer->bank_id  = $request->bankId;
        $bankTransfer->save();

//        event(new NewBankTransfer($user));

        return $this->success( trans('global.success_progress'));
    }

    public function global_social(){
        $setting =  Setting::whereIn('key',['twitter','faceBook','snapchat'])->get();

        $twitter =  $setting->where('key','twitter')->first()->body;
        $faceBook =  $setting->where('key','faceBook')->first()->body;
        $snapchat =  $setting->where('key','snapchat')->first()->body ;
        $data =  [ 'twitter' => $twitter , 'facebook' => $faceBook , 'snapchat' => $snapchat];
        return $this->success('  روابط التواصل الإجتماعي ', $data);
    }

    public function aboutUs(){
        $about_us =  Setting::where('key','about_us_ar')->first();

        $data =  $about_us ? $about_us->body : '' ;

        return $this->success('عن التطبيق ', $data);
    }

    public function terms_user(){
        $setting = Setting::where('key','terms_user_ar')->first();

         $data =  $setting ? $setting->body : '' ;

        return $this->success( 'الشروط والأحكام', $data);
    }


    public function getTypesSupport(){

        $types = TypeSupport::all();

        $data= typeSupportResource::collection($types);

        return $this->success( 'أنواع التواصل', $data);
    }

    public function contact_us(contactVaild $request ){
        $user = Auth::user();

        $support = new Support();
        $support->user_id = User::first()->id;
        $support->sender_id = $user->id;
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        event(new ContactUs(1,$user->id,$request));

        return $this->success( trans('global.message_was_sent_successfully'));
    }

    public function report(ReportVaild   $request ){
        $user = Auth::user();

        $support = new Support();
        $support->user_id = User::first()->id;
        $support->sender_id = $user->id;
        $support->type_id = 5;
        $support->message = $request->message;
        $support->save();


        return $this->success( "تم إرسال البلاغ بنجاح");
    }

}
