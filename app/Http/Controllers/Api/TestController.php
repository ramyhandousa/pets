<?php

namespace App\Http\Controllers\Api;

use App\Libraries\fastlo;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    use RespondsWithHttpStatus;

    public $fastlo ;
    public $language ;

    public function __construct(fastlo $fastlo)
    {
        $this->fastlo = $fastlo;

        $this->language = request()->headers->get('Accept-Language') ? : 'ar';
    }


    public function list_country(){

        $data =  $this->fastlo->list_cities();

        if ($data){

            $list_countries =  $this->language == "ar" ? $data['output']['cities_ar'] :  $data['output']['cities_en'];

            return $this->success("المدن",$list_countries);

        }else{

            return  $this->failure("يوجد خطأ ما للاسف");
        }
    }

    public function code_country(){

        $data =  $this->fastlo->code_cities();

        if ($data){

            $list_countries =  $this->language == "ar" ? $data['output']['cities_ar'] :  $data['output']['cities_en'];

            return $this->success("المدن",$list_countries);

        }else{

            return  $this->failure("يوجد خطأ ما للاسف");
        }
    }

    public function prices_shipment(){

        $data =  $this->fastlo->prices_shipment();

        return $this->success("أسعار التوصيل",$data['output']);
    }

    public function AddShipment(){

        $senderAddress = array(
            'use_default_address' => 0,
            'sender_name' => 'Real Sender Name',
            'sender_mobile1' => '0532120000',
            'sender_mobile2' => '',
            'sender_country' => 'SA',
            'sender_city' => 'Riyadh',
            'sender_area' => 'Rabwah',
            'sender_street' => 'شارع بولة للتسويق العقاري',
            'sender_additional' => '',
            'sender_latitude' => 0,
            'sender_longitude' => 0
        );

        $receiverAddress = array(
            'receiver_name' => 'Turki',
            'receiver_mobile1' => '0532120000',
            'receiver_mobile2' => '',
            'receiver_country' => 'SA',
            'receiver_city' => 'Riyadh',
            'receiver_area' => 'Alrimal',
            'receiver_street' => 'شارع حندوسة 2020',
            'receiver_additional' => '',
            'receiver_latitude' => 0,
            'receiver_longitude' => 0
        );

        $shipmentData = array(
            'collect_cash_amount' => 500,
            'number_of_pieces' => 1,
            'reference' => '300300',			// usually used as order number
            'mode'  => "testing"
        );

        $data =  $this->fastlo->addShipment($senderAddress,$receiverAddress,$shipmentData);

        if ($data['status_code'] == 200){

            return $this->success(" تم إضافة الشحنة   ",$data['output']);

        }else{

            return  $this->failure("برجاء التأكد من البيانات المضافة للشحنة");
        }
    }

    public function read_shipping(Request $request){

        $tracknumber = $request->tracknumber;

        if (!$tracknumber){
            return  $this->failure("برجاء إدخال رقم الشاحنة");
        }

        $data = $this->fastlo->readShipment($tracknumber);

        if ($data['status_code'] == 200){

            return $this->success(" بيانات الشحنة   ",$data['output']);

        }else{

            return  $this->failure("برجاء التأكد من رقم الشاحنة");
        }
    }


    public function get_status_shipping(Request $request){

        $tracknumber = $request->tracknumber;

        if (!$tracknumber){
            return  $this->failure("برجاء إدخال رقم الشاحنة");
        }

        $data = $this->fastlo->getShipmentsStatus($tracknumber);

        if ($data['status_code'] == 200){

            return $this->success(" بيانات الشحنة   ",$data['output']['status_list']);

        }else{

            return  $this->failure("برجاء التأكد من رقم الشاحنة");
        }
    }


    public function can_be_cancel(Request $request){
        $tracknumber = $request->tracknumber;

        if (!$tracknumber){
            return  $this->failure("برجاء إدخال رقم الشاحنة");
        }
        $data = $this->fastlo->canBeCanceled($tracknumber);

        if ($data['status_code'] == 200){

            return $this->success(" بيانات الشحنة   ",$data['output']);

        }else{

            return  $this->failure("برجاء التأكد من رقم الشاحنة");
        }

    }

    public function cancelShipment(Request $request){
        $tracknumber = $request->tracknumber;

        if (!$tracknumber){
            return  $this->failure("برجاء إدخال رقم الشاحنة");
        }

        $check_cancel_before = $this->fastlo->canBeCanceled($tracknumber);

        if ($check_cancel_before['status_code'] == 200){

            if (!$check_cancel_before['output']['can_be_canceled']){

                return  $this->failure("يبدو انه تم رفض الشحنة من قبل ");
            }
        }

        $data = $this->fastlo->cancelShipment($tracknumber);

        if ($data['status_code'] == 200){

            return $this->success(" بيانات الشحنة   ",$data['output']);

        }else{

            return  $this->failure("برجاء التأكد من رقم الشاحنة");
        }

    }



}
