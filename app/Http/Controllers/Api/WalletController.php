<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\User\VaildBankAccount;
use App\Http\Resources\Delivery\UserDeliveryResource;
use App\Http\Resources\User\userResource;
use App\Models\MasterUser;
use App\Models\Transaction;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    use RespondsWithHttpStatus ;

    public function __construct( )
    {
        $this->middleware('auth:api');
    }

    public function my_wallet(){

        $user  = Auth::user();

        $wallet = Transaction::whereUserId($user->id)->sum('price');

        $data = [
            'wallet' => $wallet,
            'bank_name' => $user->bank_name,
            'iban' =>  $user->iban,
            'account_number' =>  $user->account_number,
        ];

        return $this->success("المحفظة" , $data);
    }


    public function bank_account(VaildBankAccount $request){
        $user  = Auth::user();

        $data = collect($request->validated())->toArray();

        User::updateOrCreate(['id' => $user->id ] , $data);

        $master_user =  MasterUser::with('user')->find($user->master_user_id);

        if ($user->defined_user == "user"){

            $model = new userResource($master_user);

        }else{

            $model =  new UserDeliveryResource($master_user) ;
        }

        return $this->success("الحساب البنكي" , $model);
    }


}
