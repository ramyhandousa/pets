<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Home\ListCategoryResource;
use App\Http\Resources\lists\GeneraterceResource;
use App\Http\Resources\lists\VideoResource;
use App\Models\Category;
use App\Models\City;
use App\Models\Color;
use App\Models\CulturalVideo;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class ListController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public function categories(Request  $request){
        $lists = Category::where('is_suspend',0)->whereType($request->type)->first();

        $children = $lists ? $lists->children : [ ];

        $data = ListCategoryResource::collection($children);

        return $this->success('الأقسام', $data);
    }


    public function colours(){
        $colors = Color::all();

        return $this->success('الألوان',GeneraterceResource::collection($colors));
    }


    public function cities(){
        $cities = City::all();

        return $this->success('المدن',GeneraterceResource::collection($cities));
    }


    public function videos(Request  $request){

        $query = CulturalVideo::whereIsSuspend(0);

        $this->pagination_query($request,$query);

        $data = VideoResource::collection($query->get());

        return $this->success('الفيديوهات الثقافية',$data);
    }


}
