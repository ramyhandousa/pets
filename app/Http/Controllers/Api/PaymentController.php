<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\payment\payOrderVaild;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use App\Models\OrderOffer;
use Illuminate\Http\Request;

class PaymentController extends Controller
{

    public $push;

    public function __construct(PushNotification $push )
    {
        $this->push = $push;
    }

    public function success(payOrderVaild  $request , OrderOffer  $orderOffer){

        $order = $orderOffer->order;

        $deleteOffer = $order->offers()->where('id','!=',$orderOffer->id)->get();

        if (count($deleteOffer) > 0){  $deleteOffer->each->delete();  }

        $deleteNotify = Notification::where('order_id',$order->id)->where('user_id' ,'!=' ,  $orderOffer->delivery_id )->get();

        if (count($deleteNotify) > 0){  $deleteNotify->each->delete();  }

        $order->update([
            'is_pay' => 1 ,'delivery_id' =>  $orderOffer->delivery_id,'status' => 'current',
            'delivery_price' => $orderOffer->delivery_price ,  'paymentId' => $request->paymentId
        ]);

        $devices = Device::whereUserId($orderOffer->delivery_id)->pluck('device');

        if (count($devices) > 0){
            $this->push->sendPushNotification($devices,null,'الطلبات الجديدة ',  ' تم قبول عرضك  علي الطلب رقم ' . $order->id ,[
                'type' =>  'accepted_offer'
            ]);
        }

        return view('payment');
    }

    public function error(){

        return view('errorPayment');
    }

}
