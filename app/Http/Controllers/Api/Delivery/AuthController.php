<?php

namespace App\Http\Controllers\Api\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\checkPhoneOrEmailExist;
use App\Http\Requests\api\Auth\codeExist;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\resgister;
use App\Http\Requests\api\removeImage;
use App\Http\Requests\api\validImage;
use App\Repositories\AuthRepository;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;
    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;

        $this->middleware('auth:api')
            ->only(['changPassword' ,'editProfile' , 'edit_car','removeUser','logOut']);

        $this->middleware('vaildApiToken') ->only(['register' ]);
    }

    public function register(resgister $request){

         $this->authRepository->register($request);

        return $this->success(trans('global.register_in_successfully'));
    }


    public function login(login $request){

        $data =   $this->authRepository->login($request);

        return $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function forgetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->forgetPassword($request);

        return $this->success($data['message'],['code' => $data['code'] ] );
    }

    public function resetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->resetPassword($request);

        return $this->success($data['message'],$data['data']);
    }

    public function resendCode(codeExist $request){

        $data =    $this->authRepository->resendCode($request);

        return $this->success(trans('global.activation_code_sent'),$data);
    }

    public function checkCodeActivation(checkActivation $request){

        $data =    $this->authRepository->checkCode($request);

        return $this->success(trans('global.your_account_was_activated'),$data);
    }


    public function checkCodeCorrect(checkActivation $request){

        return $this->success('الكود صحيح');
    }


    public function changPassword ( changePassRequest  $request )
    {
        $data =    $this->authRepository->changPassword($request);

        return $this->success($data['message']);
    }


    public function editProfile (editUser $request )
    {
        $data =  $this->authRepository->editProfile($request);

        return $this->success(trans('global.profile_edit_success') , $data);
    }

    public function edit_car(Request $request )
    {
        $data =  $this->authRepository->edit_car_delivery($request);

        return $this->success(trans('global.profile_edit_success') , $data);
    }


    public function logOut(Request $request){

        $this->authRepository->logOut($request);

        return $this->success(trans('global.logged_out_successfully'));
    }

    public function upload(validImage $request){

        $data =  $this->authRepository->uploadImage($request);

        return $this->success('تم رفع الصورة بنجاح',$data);
    }

    public function removeImage(removeImage $request){

        $this->authRepository->removeImage($request);

        return $this->success('تم مسح الصورة بنجاح');
    }

    public function removeUser(Request  $request){

        $user = Auth::user();

        if ($user){
            $user->delete();

            return $this->success('تم مسح المستخدم بنجاح');
        }else{
            return $this->success('لا يابشمهندس البيانات مش صح  😂😂');
        }
    }


}
