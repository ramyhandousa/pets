<?php

namespace App\Http\Controllers\Api\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Order\Delivery\makeOffer;
use App\Http\Requests\api\Order\Delivery\offerIgnore;
use App\Http\Requests\api\Order\Delivery\orderFinish;
use App\Http\Resources\Orders\Delivery\OrderDeliveryIndex;
use App\Http\Resources\Orders\Delivery\OrderDeliveryShow;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Scoping\Scopes\LocationScope;
use App\Scoping\Scopes\OrderDeliveryStatusScope;
use App\Scoping\Scopes\OrderOfferDeliveryStatusScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class OrderDeliveryController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public function __construct( )
    {
        $this->middleware('auth:api');

        $this->middleware('vaildDelivery');
    }


    public function index(Request  $request)
    {
        $user = $request->user();

        $filter = $request->filter;

        if ($filter ==  'new' || $filter == 'pending') {

            $list =  OrderOffer::with('order')->whereDeliveryId($user->id)->withScopes($this->OrderOfferByStatus());

            $countData =  $list->count();

            $this->pagination_query($request , $list);

            $data = collect($list->get())->pluck('order')->reverse()->values();

        }else{

            $list =  Order::whereDeliveryId($user->id)->with(['pet' ,'supplie'])->withScopes($this->filterByStatus());

            $countData =  $list->count();

            $this->pagination_query($request , $list);

            $data = $list->get();

        }

        return $this->successWithPagination('طلباتي', $countData, OrderDeliveryIndex::collection($data));
    }


    protected function OrderOfferByStatus(){
        return [
            'filter'  => new OrderOfferDeliveryStatusScope(),
        ];
    }


    protected function filterByStatus(){
        return [
            'filter'  => new OrderDeliveryStatusScope(),
            'latitude'  => new LocationScope(),
        ];
    }

    public function show(Order  $order)
    {
        $data =  new OrderDeliveryShow($order);

        return $this->success('تفاصيل الطلب', $data);
    }


    public function makeOffer(makeOffer  $request, Order $order){

        $orderOffer = $order->my_delivery_offer();

        $orderOffer->update(['delivery_price' => $request->delivery_price]);

       // $order->update(['status' => 'pending']);

        return $this->success('تم تقديم سعر توصيل بنجاح' );
    }


    public function offerIgnore(offerIgnore  $request, Order $order){

        $orderOffer = $order->my_delivery_offer();

        $orderOffer->update(['ignore' => 1]);

        return $this->success('تم  تجاهل هذا الطلب بنجاح' );
    }

    public function orderFinish(orderFinish  $request, Order $order){

        $order->update(['status' => 'finish']);

        return $this->success('تم  توصيل هذا الطلب بنجاح' );
    }


    public function time_out(Order $order){
        $order->update(['time_out' => 1,'status' => 'pending']);

        return $this->success('تم   إنتهاء وقت هذا الطلب بنجاح' );
    }

}
