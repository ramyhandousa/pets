<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Home\indexResource;
use App\Http\Resources\Supplie\SupplieIndexResource;
use App\Models\Pet;
use App\Models\Supplie;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\LocationScope;
use App\Scoping\Scopes\NameScope;
use App\Scoping\Scopes\typeScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public function homePets(Request  $request){

        $query = Pet::where('is_deleted',0)->where('quantity' ,'>',0)->withScopes($this->filterData());

        $this->pagination_query($request , $query);

        return $this->success('الحيوانات',indexResource::collection($query->get())  );
    }

    public function homeSupplie(Request  $request){

        $query = Supplie::where('is_deleted',0)->where('quantity' ,'>',0)->withScopes($this->filterData());

        $this->pagination_query($request , $query);

        return $this->success('المستلزمات',indexResource::collection($query->get())  );
    }

    public function showSupplie(Supplie  $supplie){

        return $this->success('تفاصيل المستلزمات', new SupplieIndexResource($supplie)  );
    }


    protected function filterData(){
        return [
            'category'  => new CategoryScope(),
            'latitude'  => new LocationScope(),
            'query'     => new NameScope(),
            'type'     => new typeScope(),
        ];
    }
}
