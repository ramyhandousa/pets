<?php

namespace App\Http\Controllers\Api\User;

use App\Events\sendOrderToDelivery;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\Order\listOfferUser;
use App\Http\Requests\api\Order\OrderRateRequest;
use App\Http\Requests\api\Order\OrderRefuseVaild;
use App\Http\Requests\api\Order\storeRequest;
use App\Http\Resources\Orders\Users\listOfferResource;
use App\Http\Resources\Orders\Users\OrderIndexResource;
use App\Http\Resources\Orders\Users\OrderPetResource;
use App\Http\Resources\Orders\Users\OrderSupplieResource;
use App\Http\Resources\Orders\Users\ShowOrderResource;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\Pet;
use App\Models\PromoCode;
use App\Models\Rate;
use App\Models\Supplie;
use App\Scoping\Scopes\OrderStatusScope;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderUserController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public function __construct( )
    {
        $this->middleware('auth:api');
    }

    public function index(Request  $request)
    {
        $data =  Order::whereUserId($request->user()->id)->withScopes($this->filterByStatus());

        $countData =  $data->count();

        $this->pagination_query($request , $data);

        return $this->successWithPagination('طلباتي', $countData, OrderIndexResource::collection($data->get()));
    }

    protected function filterByStatus(){
        return [
            'filter'  => new OrderStatusScope(),
        ];
    }

//    public function store(storeRequest $request)
//    {
//        return $request->validated();
//        $pet = Pet::whereId($request->pet_id)->first();

//        $supplie = Supplie::whereId($request->supplie_id)->first();

//       $price =  $request->type == 'pets' ? $pet->price : $supplie->price;

//       $dataRequest = collect($request->validated())->merge(['price' => $price])->toArray();

//        $order =  $request->user()->orders()->create($dataRequest);

//        $request->type == 'pets' ?  $pet->update(['quantity' => $pet->quantity - $request->quantity]) : $supplie->update(['quantity' => $supplie->quantity - $request->quantity]);

//        event(new sendOrderToDelivery($request , $order));
//
//        $data = $request->type == 'pets' ? new OrderPetResource($order) : new OrderSupplieResource($order);
//
//        return $this->success('تم طلبك بنجاح', $data);
//    }


    public function store(storeRequest $request){

        $total_price =  $this->total_price_order($request);
        $data = [];
        $data['total_price'] = $total_price ;

        if ($request->promo_code){
            $promoCode = PromoCode::whereCode($request->promo_code)->first();
            if ($promoCode){
                $data['total_price'] = ($total_price - (($total_price * $promoCode->percent) / 100));
                $data['promo_code'] = $request->promo_code;
                $data['discount_value'] = ($total_price * $promoCode->percent) / 100;
            }
        }else{
            $data['price'] = $total_price;
        }

        $order =  $request->user()->orders()->create(array_merge($request->validated(), $data));

        OrderDetails::insert($this->map_order_details($request,$order));

        event(new sendOrderToDelivery($request , $order));

        return $this->success('تم طلبك بنجاح', new OrderIndexResource($order));
    }

    protected function total_price_order($request){
       return collect($request->order_details)->map(function ($details){
            return  $details['price'] * $details['quantity'];
        })->sum();
    }


    public function show(Order  $order)
    {

//       $data = $order->type == 'pets'? new OrderPetResource($order) : new OrderSupplieResource($order);

        return $this->success('تفاصيل طلبك بنجاح', new ShowOrderResource($order));
    }

    public function map_order_details($request,$order){

        return  collect($request->order_details)->map(function ($details) use ($order){
            return [
                'order_id'      => $order->id,
                'type'          =>  $details['type'],
                'is_supplie'    =>  $details['is_supplie'],
                'general_id'    =>  $details['general_id'],
                'price'         =>  $details['price'],
                'category_id'   =>  isset($details['category_id']) ?$details['category_id'] : null,
                'quantity'      =>  $details['quantity'],
            ];
        })->toArray();
    }


    public function listOffers(listOfferUser  $request, Order $order){

        $myOrder =  $order->load('offers_have_price.delivery');

        $offers_have_price = collect($myOrder['offers_have_price'])->sortBy('delivery_price');

        return $this->success('العروض', listOfferResource::collection($offers_have_price)) ;
    }

    public function refuseOrder(OrderRefuseVaild $request ,Order  $order){

        $order->update(['status' => 'refuse' , 'message' => $request->message]);

        return $this->success(' تم إلغاء طلبك بنجاح');
    }


    public function rateOrder(OrderRateRequest  $request,Order  $order){

        $rate = Rate::updateOrCreate(['user_id' => $request->user()->id , 'order_id' => $order->id],[
            'delivery_id' => $order->delivery_id , 'rate' => $request->rate
        ]);

        return $this->success(' تم تقييم طلبك بنجاح',['rate' => $rate->rate]);
    }
}
