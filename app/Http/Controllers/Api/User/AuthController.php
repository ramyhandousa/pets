<?php

namespace App\Http\Controllers\Api\User;

use App\Events\phoneChange;
use App\Events\UserLogOut;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\codeExist;
use App\Http\Requests\api\User\Auth\vaildRegister;
use App\Http\Resources\User\userCodeResource;
use App\Http\Resources\User\userResource;
use App\Models\Device;
use App\Models\MasterUser;
use App\Models\VerifyUser;
use App\Traits\RespondsWithHttpStatus;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct( )
    {
        $this->middleware('auth:api')->only([ 'logOut']);
    }

    public function register(vaildRegister  $request){

        $master_user = MasterUser::firstOrCreate(['phone' => $request->phone]);

        $user = User::firstOrCreate(['master_user_id' => $master_user->id,'defined_user' => 'user']);

//        $code = substr(rand(), 0, 4);
        $code = 1111;

        VerifyUser::updateOrCreate(['user_id' => $user->id], [ 'phone' => $request->phone , 'action_code'  =>  $code ]);

        return $this->success('تم تسجيل المستخدم بنجاح', new userCodeResource($user));
    }

    public function checkCodeActivation(checkActivation  $request){

        $verifyUser = VerifyUser::where('phone', $request->phone)->with(['user.master_user'])->get();

        $user =  $verifyUser->whereNotNull('user')->values()->first()->user;

        $user->master_user->update(['phone' => $request->phone ]);

        $user->update([ 'is_active' => 1]);

        $verifyUser->each->delete();

        $this->manageDevices($request, $user);

        $master_user =  MasterUser::with('user')->find($user->master_user_id);

        return $this->success('تم تفعيل المستخدم بنجاح', new userResource($master_user));
    }


    public function resendCode(codeExist $request)
    {
        $verifyUser = VerifyUser::where('phone', $request->phone)->with('user')->get();

        $user =  $verifyUser->whereNotNull('user')->values()->first();

//        $action_code = substr(rand(), 0, 4);
        $action_code = 1111;

        \event(new phoneChange($user['user'],$action_code,$request));

        return $this->success(trans('global.activation_code_sent'),['code' => $action_code ]);
    }


    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));

        return $this->success(trans('global.logged_out_successfully'));
    }


    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            $device = Device::where('device', $request->deviceToken)->first();

            if (!$device) {
                $data = new Device();
                $data->device = $request->deviceToken;
                $data->user_id = $user->id;
                $data->device_type = $request->deviceType;
                $data->save();
            }else{
                $device->update(['user_id' => $user->id , 'device' => $request->deviceToken ]);
            }
        }
    }
}
