<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Pet\editVaild;
use App\Http\Requests\api\Plant\storePlantVaild;
use App\Http\Resources\Pets\PetsIndexResource;
use App\Http\Resources\Pets\PetsResource;
use App\Models\Pet;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class PlantsController extends Controller
{
    use RespondsWithHttpStatus;

    private $petsRepository;

    public function __construct( )
    {
        $this->middleware('auth:api')->except(['show']);
    }

    public function index(Request  $request){

        $user = $request->user();

        $query =  Pet::whereDefinition('plant')->whereUserId($user->id)->where('is_deleted',0);

        $this->pagination_query($request, $query);

        return $this->success(' النباتات',PetsIndexResource::collection($query->get()) );
    }


    public function store(storePlantVaild $request){

        $data = $request->user()->pets()->create($request->validated());

        return $this->success('تم إضافة النبات بنجاح',new PetsResource($data));
    }

    public function edit(editVaild $request, $id){

        $plant = Pet::whereIsDeleted(0)->findOrFail($id);

        return $this->success('تم إعادة نشر النبات بنجاح',new PetsResource($plant));
    }

    public function destroy($id)
    {
        $plant = Pet::whereIsDeleted(0)->findOrFail($id);

        $plant->update(['is_deleted' => true]);

        return $this->success('تم مسح النبات بنجاح');
    }


    public function pagination_query($request ,$query ){
        $pageSize = $request->pageSize ?: 10;
        $skipCount = $request->skipCount;
        $currentPage = $request->get('take', 1); // Default to 1
        $query->skip($skipCount + (($currentPage - 1) * $pageSize));
        $query->take($pageSize);
        return $query->latest();
    }


}
