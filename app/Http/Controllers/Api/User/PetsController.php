<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Pet\editVaild;
use App\Http\Requests\api\Pet\storeVaild;
use App\Http\Resources\Pets\PetsIndexResource;
use App\Http\Resources\Pets\PetsResource;
use App\Models\Pet;
use App\Repositories\PetRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class PetsController extends Controller
{
    use RespondsWithHttpStatus;

    private $petsRepository;

    public function __construct(PetRepository $petsRepository)
    {
        $this->petsRepository = $petsRepository;

        $this->middleware('auth:api')->except(['show']);
    }

    public function index(Request $request)
    {
       $data =  $this->petsRepository->list($request);

        return $this->success(' الحيوانات',PetsIndexResource::collection($data) );
    }

    public function store(storeVaild $request)
    {
        $data =  $this->petsRepository->storePet($request);

        return $this->success('تم إضافة الحيوان بنجاح',new PetsResource($data));
    }


    public function show(Pet  $pet)
    {
        Pet::whereIsDeleted(0)->findOrFail($pet->id);

        return $this->success('تفاصيل   الحيوان  ',new PetsResource($pet));
    }


    public function update(editVaild $request,Pet $pet)
    {
        Pet::whereIsDeleted(0)->findOrFail($pet->id);

        $data =  $this->petsRepository->editPet($request ,$pet);

        return $this->success('تم إعادة نشر الحيوان بنجاح',new PetsResource($data));
    }


    public function destroy(Pet  $pet)
    {
        Pet::whereIsDeleted(0)->findOrFail($pet->id);

        $this->petsRepository->deletePet($pet);

        return $this->success('تم مسح الحيوان بنجاح');
    }
}
