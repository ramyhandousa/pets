<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{




    public function __construct()
    {


    }

    public function index()
    {

         if (!Auth::guard('master_user')->check())
            return redirect(route('admin.login'));


        return view('admin.home.index');
    }
}
