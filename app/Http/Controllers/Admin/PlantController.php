<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlantController extends Controller
{
    public function index(){

        $pets = Pet::where('definition','plant')
            ->join('cities', 'pets.city_id', '=', 'cities.id')
            ->join('categories', 'pets.category_id', '=', 'categories.id')
//            ->join('colors', 'pets.color_id', '=', 'colors.id')
            ->join('users', 'pets.user_id', '=', 'users.id')
            ->join('master_users','users.master_user_id','=','master_users.id')
            ->select('pets.id','pets.name','price','quantity','weight','pets.is_suspend',
                'cities.name_ar as city_name' ,
                'categories.name_ar as category_name', 'master_users.phone as user_phone',
                DB::raw("(CASE WHEN (type = 'male') THEN 'ذكر' ELSE 'أنثي' END) as type")
            )
            ->where('is_deleted' , 0)
            ->get();


        $pageName  = 'النباتات';

        return view('admin.plant.index',compact('pets','pageName'));
    }
}
