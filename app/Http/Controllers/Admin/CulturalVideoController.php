<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VaildVideoUpload;
use App\Models\CulturalVideo;
use Illuminate\Http\Request;
use App\Http\Helpers\Images;
use Illuminate\Support\Facades\File;

class CulturalVideoController extends Controller
{
    public function index(){
        $videos = CulturalVideo::select('id','title','is_suspend','created_at')->get();
        $pageName = 'الفيديوهات';
        return view('admin.cultural_videos.index',compact('videos','pageName'));
    }

    public function show(){

    }

    public function edit($id){

        $video = CulturalVideo::findOrFail($id);

        $pageName = 'تعديل الفيديو ';

        return view('admin.cultural_videos.edit',compact('video','pageName'));
    }

    public function create(){

        return view('admin.cultural_videos.create');
    }

    public function store(VaildVideoUpload  $request){

        $video = CulturalVideo::create($request->only(['title','desc','url']));

        $video->update(['file' => 'videos/'. Images::uploadVideo($request, 'file','/videos/')]);

        if($request->file('image') && $request->image != null){
            $video->update(['image' => 'files/'. Images::uploadVideo($request, 'image','/files/')]);
        }
        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => 'الفيديو']),
            "url" => route('cultural_videos.index'),
        ]);
    }

    public function update(VaildVideoUpload  $request, $id){

        $video = CulturalVideo::find($id);

        $video->update($request->only(['title','desc','url']));

        if ($request->file){

            if ($video->file){
                File::delete(public_path().'/'.$video->file);
            }
            $video->update(['file' => 'videos/'. Images::uploadVideo($request, 'file','/videos/')]);
        }

        if($request->file('image') && $request->image != null){
            if ($video->image){
                File::delete(public_path().'/'.$video->image);
            }
            $video->update(['image' => 'files/'. Images::uploadVideo($request, 'image','/files/')]);
        }

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'الفيديو']),
            "url" => route('cultural_videos.index'),
        ]);

    }

    public function delete(Request $request){
        $video = CulturalVideo::findOrFail($request->id);

        if ($video->file){
            File::delete(public_path().'/'.$video->file);
        }

        if ($video->image){
            File::delete(public_path().'/'.$video->image);
        }

        $video->delete();

        return response()->json([
            'status' => true,
            'data' => $video->id
        ]);
    }

    public function suspend(Request $request)
    {
        $model = CulturalVideo::findOrFail($request->id);

        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type
            ]);
        }

    }
}
