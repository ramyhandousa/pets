<?php

namespace App\Http\Controllers\Admin;


use App\Http\Helpers\Images;
use App\Models\MasterUser;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Silber\Bouncer\Database\Role;
use Validator;
use Illuminate\Support\Facades\Gate;

class HelpAdminController extends Controller
{
    public $public_path;

    public function __construct()
    {
        $this->public_path = 'files/';

    }

    public function index(Request $request)
    {
        $id = Auth::guard('master_user')->user()->id;

	    $users = MasterUser::where('id', '!=', $id)->whereHas('roles', function ($q) {
            $q->where('name', '!=', '*');

        })->with('roles')->latest()->get();



        return view('admin.helpAdmin.index', compact('users'));
    }



    public function create()
    {
        $roles = Role::get();

        $roles = $roles->reject(function ($q) {
            return $q->name == '*';
        });

        return view('admin.helpAdmin.create', compact('roles'));
    }


    public function store(Request $request)
    {

        //Get input ...
        $post_data = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'phone' => $request->phone,
            'email' => $request->email,
            'password' => $request->password,
            'confirm_password' => $request->password_confirmation,
            'image' => $request->image,
        ];

        //set Rules .....
        $valRules = [
            'phone' => 'required|unique:master_users',
            'email' => 'required|unique:master_users',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
             'image' => 'image|mimes:jpeg,png,jpg|max:1000'
        ];

        //Declare Validation message
        $valMessages = [
            'first_name.required' => 'إسم الاول مطلوب',
            'last_name.required' => 'إسم الاخير مطلوب',
            'name.unique' => 'هذا الإسم مستخدم من قبل , حاول بإسم آخر',
            'phone.required' => 'رقم الهاتف مطلوب',
            'phone.unique' => 'هذا الرقم مستخدم من قبل',
            'email.required' => 'البريد الإلكتروني مطلوب',
            'email.unique' => 'هذا البريد مستخدم من قبل',
            'password.required' => 'كلمة المرور مطلوبة',
            'confirm_password.same' => 'كلمة المرور غير متطابقة',
             'image' => 'يجب إضافة صورة لإضافة مدير وتكون بصيغة PNG او JPG',
            'image.max' => '  اقصي حجم مسموح به هو 1 ميجا للصورة   ',
        ];

        //validate inputs ......
        $valResult = Validator::make($post_data, $valRules, $valMessages);
        if ($valResult->passes()) {

            $master_user = MasterUser::create(['phone' => $request->phone , 'email' => $request->email,'password' => $request->password]);

            $user                   = new User;
            $user->master_user_id   = $master_user->id;
            $user->defined_user     = 'helper_admin';
            $user->name             = $request->name;
            $user->is_active        = 1;
            if ($request->hasFile('image')):
                $user->image =  $this->public_path   .Images::uploadImage($request,'image',$this->public_path);
            endif;
            $user->save();

            foreach ($request->input('roles') as $role) {
                $master_user->assign($role);
            }

            session()->flash('success', 'لقد تم إضافة مساعد لك بنجاح.');
            return redirect()->route('helpAdmin.index');
        } else {
            // Grab Messages From Validator
            $valErrors = $valResult->messages();
            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);
        }
    }

    public function show($id)
    {
        $roles = Role::get();

        $user = MasterUser::findOrFail($id);

        return view('admin.helpAdmin.show', compact('user', 'roles'));
    }

    public function edit($id)
    {
        $roles = Role::get();

        $roles = $roles->reject(function ($q) {
            return $q->name == '*';
        });

        $user = MasterUser::findOrFail($id);

        return view('admin.helpAdmin.edit', compact('user', 'roles'));
    }

    public function update(Request $request, $id)
    {

        $user = User::with('master_user')->findOrFail($id);

        $master_user = $user['master_user'];

        $postData = $this->postData($request);

        // Declare Validation Rules.
        $valRules = $this->valRules($master_user->id);

        // Declare Validation Messages
        $valMessages = $this->valMessages();

        // Validate Input
        $valResult = Validator::make($postData, $valRules, $valMessages);

        // Check Validate
        if ($valResult->passes()) {


            $user->name = $request->name;
            $master_user->email = $request->email;
            $master_user->phone = $request->phone;


            if ($request->password) {
                $master_user->password = $request->password;
            }
//            $user->address = $request->address;
            if ($request->hasFile('image')):

                $user->image = $this->public_path   .Images::uploadImage($request,'image',$this->public_path);

                if (isset($request->oldImage) && $request->oldImage != '') {
                    $regularPath = str_replace($request->root() . '/public/', '', $request->oldImage);

                    if (\File::exists(public_path($regularPath))):
                        \File::delete(public_path($regularPath));
                    endif;

                }
            endif;


            $user->save();
            $master_user->save();


            if ($request->input('roles')) {

                foreach ($master_user->roles as $role) {
                    $master_user->retract($role);
                }

                foreach ($request->input('roles') as $role) {
                    $master_user->assign($role);
                }
            }

            session()->flash('success', "لقد تم التعديل بنجاح");

            return redirect()->back();
        } else {

            $valErrors = $valResult->messages();

            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);

        }
    }


    public function showMessage($id)
    {
        if (!Gate::allows('admin_manage')) {
            return abort(401);
        }

        $user = User::find($id);
        return view('admin.helpAdmin.message', compact('user'));
    }


    public function destroy($id)
    {
        if (!Gate::allows('admin_manage')) {
            return abort(401);
        }


        $user = User::findOrFail($id)->delete();

        return redirect()->route('admin.helpAdmin.index');
    }

    public function delete(Request $request)
    {
        if (!Gate::allows('admin_manage')) {
            return abort(401);
        }

        $user = User::findOrFail($request->id);
        return view('admin.helpAdmin.delete', compact('user'));

    }

    public function deleteHelpAdmin(Request $request)
    {
        if (!Gate::allows('admin_manage')) {
            return abort(401);
        }

        $validator = Validator::make($request->all(), ['deleted' => 'required|max:255',]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::findOrFail($request->id);
        $message = $request->deleted;

        // Delete User for view not in database and make message why Deleted

        if ($user->update(['is_deleted' => 1, 'message' => $message])) {

            session()->flash('success', "لقد تم مسح المستخدم ($user->name) بنجاح");

            // return back for request -> coming  From ( $request->type )

            return redirect()->route('helpAdmin.index');
        }


    }

    public function suspend(Request $request)
    {
        $model = User::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {


            $message = "لقد تم حظر  بنجاح";

        } else {
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }

    public function suspendHelpAdmin(Request $request)
    {

        if (!Gate::allows('admin_manage')) {
            return abort(401);
        }

        $validator = Validator::make($request->all(), ['suspend' => 'required|max:255',]);

        if ($validator->fails()) {

            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user = User::findOrFail($request->id);


        $message = $request->suspend;

        // Suspend User for view not in database and make message why Suspend and make User not active

        if ($user->update(['is_active' => config('constants.status.suspended'), 'message' => $message])) {

            session()->flash('success', "لقد تم حظر المستخدم ($user->name) بنجاح");

            // return back for request -> coming  From ( $request->type )

            return redirect()->route('helpAdmin.index');
        }


    }


    private function postData($request)
    {
        return [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
        ];
    }

    /**
     * @return array
     */
    private function valRules($id)
    {
        return [
             'name' => 'required',
            'email' => 'required|email|unique:master_users,email,' . $id,
            'phone' => 'required|unique:master_users,phone,' . $id,
            'password_confirmation' => 'same:password'
        ];
    }

    /**
     * @return array
     */
    private function valMessages()
    {
        return [
            'name.required' => trans('global.field_required'),
            'name.unique' => trans('global.unique_name'),
            'email.required' => trans('global.field_required'),
            'email.unique' => trans('global.unique_email'),
            'phone.required' => trans('global.field_required'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }


}
