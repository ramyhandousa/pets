<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\Database\Ability;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use Validator;

class RolesController extends Controller
{
    public function index(Request $request)
    {
        $page_title = 'إدارة الأدوار';
        $roles = Role::where('name','!=','*')->get();

        return view('admin.roles.index')->with(compact('roles','page_title'));
    }


    public function create()
    {

        $abilities = Ability::get();

        $abilities = $abilities->filter(function ($q) {
            return $q->name !== '*';
        });

        return view('admin.roles.create', compact('abilities'))->render();
    }


    public function store(Request $request)
    {

        //Get input ...
        $post_data=['name'=>$request->name,];

        //set Rules .....
        $valRules = ['name'=>'required|unique:roles,name,',];

        //Declare Validation message
        $valMessages = [
            'name.required'=>'إسم الدور مطلوب',
            'name.unique'=>'هذا الإسم مستخدم من قبل , حاول بإسم آخر'
        ];

        //validate inputs ......
        $valResult = Validator::make($post_data,$valRules,$valMessages);

        if($valResult->passes()) {

            $collection = collect($request->input('abilities'));

            $abilites = $collection->filter(function ($value) {
                return $value != "*";
            })->values();


            $role = Role::create($request->all());

            $role->allow($abilites);

            session()->flash('success', 'لقد تم إضافة دور جديد للمستخدمين بنجاح.');

            return redirect(route('roles.index'));

        }else{
            // Grab Messages From Validator
            $valErrors = $valResult->messages();
            // Error, Redirect To User Edit
            return redirect()->back()->withInput()
                ->withErrors($valErrors);
        }
    }


    public function edit($id)
    {
        $abilities = Ability::get();

        $abilities = $abilities->filter(function ($q) {
            return $q->name != '*';
        });


        $role = Role::findOrFail($id);

        return view('admin.roles.edit', compact('role', 'abilities'));
    }


    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);

        $role->title = $request->name;
        $role->name =   $request->name;
//        $role->title_en = $request->title_en;
        if ($role->save()) {
            //$role->update($request->all());
            $collection = collect($request->input('abilities'));

            $abilites = $collection->filter(function ($value) {
                return $value != "*";
            })->values();
            if ($abilites->count() > 0) {

                foreach ($role->getAbilities() as $ability) {
                    $role->disallow($ability->id);
                }
                $role->allow($abilites);
            }
        }

        session()->flash('success', "لقد تم تعديل الدور  ($role->title) بنجاح");
        return redirect(route('roles.index'));
    }



    public function destroy($id)
    {
        if (!Gate::allows('users_manage')) {
            return abort(401);
        }
        $role = Role::findOrFail($id);

        if ($role->users->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => 'عفواً, لا يمكنك حذف الدور نظراً لوجود مستخدمين مشتركين فيه'
            ]);
        }


        foreach ($role->getAbilities() as $ability) {
            $role->disallow($ability->id);
        }

        if ($role->delete()) {
            return response()->json([
                "status" => true,

            ]);
        }

    }


    public function delete(Request $request)
    {
        $role = Role::findOrFail($request->id);

        if ($role->users->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => 'عفواً, لا يمكنك حذف الدور نظراً لوجود مستخدمين مشتركين فيه'
            ]);
        }

        foreach ($role->getAbilities() as $ability) {
            $role->disallow($ability->id);
        }

        if ($role->delete()) {
            return response()->json([
                'status' => true,
                'data' => [
                    'id' => $request->id
                ],
                'message' => 'لقد تم عمليه الحذف بنجاح'
            ]);
        }
    }


    function filter(Request $request)
    {

        $name = $request->keyName;

        $page = $request->pageSize;

        ## GET ALL CATEGORIES PARENTS
        $query = Role::select();
        // $categories = Category::paginate($pageSize);


        if ($name != '') {
            $query->where('name', 'like', "%$name%");
        }

        $query->orderBy('created_at', 'DESC');
        $roles = $query->paginate(($page) ?: 10);

        if ($name != '') {
            $roles->setPath('roles?name=' . $name);
        } else {
            $roles->setPath('roles');
        }


        if ($request->ajax()) {
            return view('admin.roles.load', ['roles' => $roles])->render();
        }
        ## SHOW CATEGORIES LIST VIEW WITH SEND CATEGORIES DATA.
        return view('admin.roles.index')
            ->with(compact('users'));
    }

    public function groupDelete(Request $request)
    {

        $ids = $request->ids;
        foreach ($ids as $id) {
            $role = Role::findOrFail($id);
            $role->delete();
        }


        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ]
        ]);
    }


    public function massDestroy(Request $request)
    {

        if ($request->input('ids')) {
            $entries = Role::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
