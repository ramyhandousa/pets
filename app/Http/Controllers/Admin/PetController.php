<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PetController extends Controller
{

    public function index()
    {
        $pets = Pet::join('cities', 'pets.city_id', '=', 'cities.id')
            ->join('categories', 'pets.category_id', '=', 'categories.id')
            ->join('colors', 'pets.color_id', '=', 'colors.id')
            ->join('users', 'pets.user_id', '=', 'users.id')
            ->join('master_users','users.master_user_id','=','master_users.id')
            ->select('pets.id','pets.name','price','quantity','weight','pets.is_suspend',
                'cities.name_ar as city_name' , 'colors.name_ar as color_name',
                'categories.name_ar as category_name', 'master_users.phone as user_phone',
                DB::raw("(CASE WHEN (type = 'male') THEN 'ذكر' ELSE 'أنثي' END) as type")
            )
            ->where('is_deleted' , 0)
            ->get();

        $pageName  = 'الحيوانات';

        return view('admin.pets.index',compact('pets','pageName'));
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }


    public function suspend(Request $request)
    {
        $model = Pet::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم حظر الحيوان بنجاح";
        } else {
            $message = "لقد تم فك الحظر على الحيوان بنجاح";
        }
        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type
            ]);
        }


    }
}
