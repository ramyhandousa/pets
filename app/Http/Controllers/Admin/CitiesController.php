<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use App\Models\Countery;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;

class CitiesController extends Controller
{


    public function index(Request $request)
    {
        $cities = City::latest()->get();

        $pageName = 'المدن';

        return view('admin.cities.index',compact('cities','pageName'));
    }


    public function create(Request $request)
    {
        $pageName = 'المدن';

        return view('admin.cities.create',compact('pageName'));
    }

    public function store(Request $request)
    {

        $city = new City;
        $city->name_ar = $request->name_ar;
        $city->save();

        if ($request->type == 'countery'){
            $url =  route('cities.index').'?type=countery';
            $name = 'المدينة';
        }else{
            $url =  route('cities.index');
            $name = 'الحي';
        }
        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => $name]),
            "url" => $url,
        ]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id,Request $request)
    {
        $city = City::findOrFail($id);

        $pageName = 'المدينة';

        return view('admin.cities.edit',compact('city','pageName'));
    }


    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);

        $city->name_ar = $request->name_ar;
        $city->save();

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => 'المدينة']),
            "url" => route('cities.index'),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function groupDelete(Request $request)
    {

        $ids = $request->ids;

        $arrsCannotDelete = [];
        foreach ($ids as $id) {
            $model = City::findOrFail($id);

            if ($model->companies->count() > 0) {
                $arrsCannotDelete[] = $model->name;
            } else {
                $model->delete();
            }
        }

        return response()->json([
            'status' => true,
            'data' => [
                'id' => $request->id
            ],
            'message' => $arrsCannotDelete
        ]);
    }

    public function delete(Request $request)
    {
        $model = City::findOrFail($request->id);

        if ($model->pets->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => "عفواً, لا يمكنك حذف المدينة لوجود حيوانات بها"
            ]);
        }

        if ($model->supplies->count() > 0) {
            return response()->json([
                'status' => false,
                'message' => "عفواً, لا يمكنك حذف المدينة لوجود مستلزمات بها"
            ]);
        }

        if ($model->delete()) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }


    public function suspend(Request $request)
    {
        $model = City::findOrFail($request->id);
        $model->is_suspend = $request->type;
        if ($request->type == 1) {

            $message = "لقد تم الحظر على مستوي النظام بنجاح";

        } else {
            $message = "لقد تم فك الحظر  بنجاح";
        }

        if ($model->save()) {
            return response()->json([
                'status' => true,
                'message' => $message,
                'id' => $request->id,
                'type' => $request->type

            ]);
        }

    }


}
