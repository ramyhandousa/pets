<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\Admin\sendPromoCodeVaild;
use App\Models\Device;
use App\Models\Notification;
use App\Models\PromoCode;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\PushNotification;
use Carbon\Carbon;
class NotificationsController extends Controller
{


    public $push;

    public function __construct(PushNotification $push )
    {

        $this->push = $push;

        Carbon::setLocale(config('app.locale'));

    }

    public function index()
    {
         $notifications = auth()->user()->notifications()->orderBy('created_at', 'desc')->get();

        return view('admin.notifications.index')->with(compact('notifications'));
    }


    public function promo_code(){

        $codes = PromoCode::whereIsSuspend(0)->whereIsDeleted(0)->whereDate('end_at', '>', Carbon::now())->get();

        if ($codes->count() == 0) {
            session()->flash("myErrors","للأسف لا يوجد حاليا اكواد خصم لارسالها ");
            return  redirect()->back();
        }
        return view('admin.notifications.promo_code',compact('codes'));
    }

    public function get_promo_code(Request  $request){
        $code = PromoCode::whereCode($request->id)->first();

        return response()->json([
            'status' => 200,
            'message' =>    "نسبة الخصم علي هذا الكود "  .$code->percent . " % " ,
        ],200);
    }

    public function publicNotifications()
    {

        $notifications = Notification::whereType(15)->with('user')->get();

        return view('admin.notifications.list')->with(compact('notifications'));

    }


    public function createPublicNotifications()
    {
        return view('admin.notifications.public');
    }


    public function sendPublicNotifications(Request $request)
    {
//        Notification::create([ 'topic' => 'all','title'=>  $request->title,'body'=> $request->message ,'type'=> 15]);

        $data = [
            'title'=>  $request->title,
            'body'=> $request->message
        ];

        $this->push->sendPushNotification(null, null, $request->title,$request->message, $data,'topic' );
        session()->flash('success', __('trans.successSendPublicNotifications'));

       return redirect()->back();
    }


    public function getNotifications()
    {
        $user = auth()->user();
        $notifications = $user->notifications;
        return view('admin.notifications.index')->with(compact('notifications'));

    }

    public function send_promo_code(sendPromoCodeVaild  $request){

        $message = $request->message . " " . $request->code;

        $data = ['title'=>  $request->title, 'body'=> $message ];

        if ($request->type === "topic"){
            Notification::create(['topic' => $request->type , 'title' => $request->title , 'body' => $message , 'type' => 12]);

            $this->push->sendPushNotification(null, null, $request->title,$message, $data,'topic' );

        }else{

            Notification::insert($this->collect_data_users($request));

            $devices = Device::whereHas('user',function ($user) use ($request){
                $user->whereIn('id', $request->users);
            })->pluck('device');

            $this->push->sendPushNotification($devices, null, $request->title,$message, $data );
        }

        session()->flash('success',  " تم إرسال كود الخصم بنجاح ");
        return redirect()->back();
    }

    public function collect_data_users($request){
        $data = [];
        foreach ($request->users as $user){
            $data[] = $user;
        }
        return collect($data)->map(function ($q) use ($request){
            return [
                "user_id" => $q,
                "title" => $request->title,
                "body" => $request->message . "   ". $request->code,
                "type" =>  12,
            ];
        })->toArray();
    }

    public function renderUsesData(Request  $request){

        $query = User::where('is_active',1)->where('is_suspend',0);

        if ($request->type == "delivery"){
            $query->where('defined_user', "delivery");
        }else{
            $query->where('defined_user', "user");
        }

        $users = $query->get();

        if (count($users) > 0){
            return response()->json([
                'status' => 200,
                'html' => view('admin.notifications.show_users' , compact('users'))->render(),
            ]);
        }
        return response()->json([
            'status' => 400,
            'message' => "للاسف لا يوجد اي عدد متاح",
            "type" => $request->type
        ],400);

    }

    public function delete(Request $request)
    {

        $model = Notification::whereId($request->id)->first();


        if ($model->delete()) {
            return response()->json([
                'status' => true,
                'data' => $request->id
            ]);
        }

    }


}
