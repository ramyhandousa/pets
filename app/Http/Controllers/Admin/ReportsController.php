<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportsController extends Controller
{

    public function index(Request $request)
    {
        $pageName = 'تقارير إدارة الطلبات';

        $filterUser = function ($q){
          $q->select('id','phone');
        };
        $filterSelect = function ($q){
          $q->select('id','name','city_id','category_id');
        };

        $query = Order::query();

        $reports = $this->filter_query($request, $query);

        $reports = $reports->select('id','user_id','delivery_id','pet_id','supplie_id','quantity','is_pay','status')
            ->with(['user.master_user' => $filterUser,'delivery.master_user' => $filterUser,
            'pet' => $filterSelect,'supplie' => $filterSelect])->latest()->get();

        return view('admin.Reports.index', compact('reports','pageName'));
    }

    public function  show($id){

       $order = Order::findOrFail($id);

       if ($order->pet_id != null ){
           $section_name = optional($order->pet)->definition == "animal" ? " الحيوان " : " النبات  ";
       }else{
           $section_name = optional($order->supplie)->definition == "animal" ? " الحيوان" : "النبات  ";
       }
       return view("admin.Reports.show",compact("order","section_name"));
    }


    public function filter_query($request , $query){
        if ($request->type == "pets"){
            $reports   = $query->whereNotNull("pet_id")->whereHas("pet",function ($pet){
                $pet->where("definition","animal")->where('is_deleted',0);
            });
        }elseif ($request->type == "pets_supplies"){
            $reports   = $query->whereNotNull("supplie_id")->whereHas("supplie",function ($pet){
                $pet->where("definition","animal")->where('is_deleted',0);
            });

        }elseif ($request->type == "plants"){

            $reports   = $query->whereNotNull("pet_id")->whereHas("pet",function ($pet){
                $pet->where("definition","plant")->where('is_deleted',0);
            });

        }else{

            $reports   = $query->whereNotNull("supplie_id")->whereHas("supplie",function ($pet){
                $pet->where("definition","plant")->where('is_deleted',0);
            });

        }

        return $reports;
    }




}
