<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\editSupplies;
use App\Http\Requests\Admin\storeSupplies;
use App\Models\Category;
use App\Models\City;
use App\Models\Supplie;
use App\Models\UploadImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class SuppliesController extends Controller
{

    public function index()
    {
        $supplies = Supplie::join('cities', 'supplies.city_id', '=', 'cities.id')
            ->join('categories', 'supplies.category_id', '=', 'categories.id')
            ->select('supplies.id','definition','name','quantity','price',
                'cities.name_ar as city_name' ,'categories.name_ar as category_name')
            ->where('is_deleted' , 0)
            ->get();

        $pageName = 'المستلزمات';

        return view('admin.supplies.index',compact('supplies', 'pageName'));
    }


    public function create()
    {
        $categories = Category::whereNotIn('parent_id',[ 0,1])->latest()->get();
        $cities = City::latest()->get();
        $pageName = 'إضافة مستلزم';
        return view('admin.supplies.create', compact('categories','cities', 'pageName'));
    }


    public function store(storeSupplies $request)
    {
        $images_uploads = [];

        foreach ($request->images as $image){
            $imageName = $filename = time() . '.' . Str::random(20) . $image->getClientOriginalName();
            $image->move(public_path('files'), $imageName);

            $images_uploads[] = 'files/'.$imageName;
        }

        $supplie = Supplie::create(collect($request->validated())->except('images')->toArray());
        $supplie->update(['images' => $images_uploads]);

        return response()->json([
            'status' => true,
            "message" => __('trans.addingSuccess',['itemName' => "المستلزم"]),
            "url" =>  route('supplies.index'),
        ]);

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $supplie = Supplie::findOrFail($id);


        $categories = Category::whereNotIn('parent_id',[ 0,1])->latest()->get();
        $cities = City::latest()->get();
        $pageName = 'تعديل مستلزم';

        return view('admin.supplies.edit', compact('categories','cities', 'pageName','supplie'));
    }


    public function update(editSupplies $request, $id)
    {

        $supplie = Supplie::findOrFail($id);

        Supplie::findOrFail($id)->update(collect($request->validated())->except('images')->toArray());

        if ($request->images){
            $images_uploads = [];

            foreach ($request->images as $image){
                $imageName = $filename = time() . '.' . Str::random(20) . $image->getClientOriginalName();
                $image->move(public_path('files'), $imageName);

                $images_uploads[] = 'files/'.$imageName;
            }
            $supplie->update(['images' => $images_uploads]);
        }

        return response()->json([
            'status' => true,
            "message" => __('trans.editSuccess',['itemName' => "المستلزم"]),
            "url" =>  route('supplies.index'),
        ]);

    }


    public function delete(Request $request)
    {
        $model = Supplie::findOrFail($request->id);

        if ($model->update(['is_deleted' => 1])) {

            return response()->json([
                'status' => true,
                'data' => $model->id
            ]);
        }
    }



    public function deleteImage(Request  $request ){

        $model = Supplie::findOrFail($request->id);

        if ($request->file){
                File::delete(public_path().'/'.$request->file);
        }

        $images =  collect($model->images)->filter(function ($value) use ($request){
            return $value != $request->file;
        });

        $model->update(['images' => $images]);

        return response()->json([
            'status' => true,
            'data' => $model->id
        ]);
    }


}
