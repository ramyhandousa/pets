<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{

    public function index(Request  $request)
    {
        $query = User::select('users.id','name','phone','email','is_accepted','is_suspend','users.created_at')->where('defined_user','delivery');

        if (isset($request->is_accepted)){
            $query->whereIsActiveAndIsAccepted(1,0);
            $pageName  = 'طلبات الإنضمام';
        }else{
            $query->whereIsActiveAndIsAccepted(1,1);
            $pageName  = 'المناديب لدينا';
        }

        $delivery =  $query->join('master_users', 'users.master_user_id', '=', 'master_users.id')->get();

        return view('admin.delivery.index',compact('delivery','pageName'));
    }


    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $user = User::with('master_user','profile')->findOrFail($id);


        return view('admin.delivery.show',compact('user'));
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function accpetedUser(Request $request){

        $user = User::findOrFail($request->id);

        if ($user){

            $user->update(['is_accepted' => 1]);

            return response()->json( [ 'status' => true  ] , 200 );

        }else{

            return response()->json( [   'status' => false  ] , 200 );
        }
    }


    public function refuseUser(Request $request){
        $user = User::findOrFail($request->id);

        if ($user){
//            $user->delete();
        }
        return response()->json( [  'status' => true , ] , 200 );
    }
}
