<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\GymValid;
use App\Libraries\PushNotification;
use App\Mail\AcceptedAccount;
use App\Mail\refuseAccount;
use App\Notifications\sendEmailToNewAccount;
use App\User;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Silber\Bouncer\Database\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use UploadImage;
use Validator;

class UsersController extends Controller
{

    public $public_path;
    public $domain;
    public $push;

    public function __construct(PushNotification $push)
    {
        $this->public_path = 'files/users/';
        $this->domain = URL('/').'/';
        $this->push = $push;
    }

    public function index(Request $request)
    {

        $users = User::select('users.id','is_suspend','master_users.phone','users.created_at')
            ->where('is_active', 1)->where('defined_user','user')
            ->join('master_users', 'users.master_user_id', '=', 'master_users.id')
            ->withCount('pets','orders')->get();

        $pageName = 'إدارة المستخدمين';

        return view('admin.users.index', compact('users' , 'pageName'));
    }



    public function create()
    {

        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.add', compact('cities'));
    }

    public function testImageView(Request $request){



        $cities = City::where('is_suspend',0)->whereParentId(null)->get();
        return view('admin.users.testing', compact('cities'));
    }


    public function testImage(Request $request){

        return $request->all();
    }

    public function show($id)
    {
        $user = User::with('profile')->findOrFail($id);

        if ($user['profile']){
            $user['profile']['cv'] = $this->getImage($user['profile']['cv']);
            $user['profile']['images_work'] = $this->getAllImage($user['profile']['images_work']);
        }

        return view('admin.users.show', compact('user' ));
    }

    function getImage($id){
        $image = \App\Models\UploadImage::whereId($id)->first();

        return $image ? $this->domain. $image->image : null;
    }

    function getAllImage($ids){
        if ($ids)
        return \App\Models\UploadImage::whereIn('id',$ids)->get(['image'])->pluck('image');

    }

    public function accpetedUser(Request $request){

        $user = User::findOrFail($request->id);

        if ($user){

            $user->update(['defined_user' => 'designer','is_accepted' => 1]);

            try {
//                Mail::to($user->email)->send(new AcceptedAccount($user, $request));

            } catch (\Exception $e) {

            }

            return response()->json( [
                'status' => true ,
            ] , 200 );

        }else{

            return response()->json( [
                'status' => false
            ] , 200 );

        }

    }



    public function refuseUser(Request $request){
        $user = User::findOrFail($request->id);

        $user->update(['is_accepted' => -1,'message' => $request->message]);

        try {
//                Mail::to($user->email)->send(new refuseAccount($user));

        } catch (\Exception $e) {

        }

        return response()->json( [
            'status' => true ,
        ] , 200 );

//        session()->flash('success', 'لقد تم  رفض مزود الخدمة  بنجاح.');
//        return redirect()->route('users.index','type=provider');
    }




    public function reAcceptUser(Request $request){
        $user = User::findOrFail($request->id);

        $user->update([  'is_accepted' => 1,'message' => null]);

        try {

//            Mail::to($user->email)->send(new AcceptedAccount($user, $request));

        } catch (\Exception $e) {
            // report($e);

            // return false;
        }

        return response()->json( [
            'status' => true ,
        ] , 200 );
    }


    public function suspendUser(Request $request)
    {
        $model = User::with('devices')->findOrFail($request->id);
        $devices = $model['devices']->pluck('device');

        $model->is_suspend = $request->type;
        $model->message = $request->message;
        if ($request->type == -1) {
            if ($devices){
                collect($model['devices'])->each->delete();
            }
            $this->push->sendPushNotification($devices,null,'حظر الإدارة',  ' تم حظرك من الإدارة بسبب ' . $request->message ,[
                'type' =>  'logout'
            ]);
            $model->is_suspend = 1;
            $model->api_token = Str::random(60);
            $message = "لقد تم حظر  بنجاح";

        } else {

            $model->message = null;
            $message = "لقد تم فك الحظر بنجاح";
        }

        if ($model->save()) {
            session()->flash('success',$message);
            return redirect()->back();
        }

    }





}
