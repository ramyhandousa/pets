<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class notifyBeforeOrderComing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifyOrder:notifyBeforeOrderComing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    public $push;
    public  $notify;


    public function __construct(InsertNotification $notification ,PushNotification $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $minutes_control = Setting::whereKey('minutes_control')->first();

        $orders = Order::whereHas('user')->whereStatus('new')->where('time_out',0)
            ->with('user.devices')->with('offers_have_price')->get();


        $now = Carbon::now()->format('Y-m-d H:i');

        $addMinutest = $minutes_control ? (int)$minutes_control->body : 1;

        if (count($orders) > 0)

            foreach ($orders as $order):

                $orderTime = Carbon::parse($order->created_at)->addMinutes( $addMinutest  )->format('Y-m-d H:i');
                if(    $orderTime == $now ):

                    $offerCount = count($order['offers_have_price']);

                    if ( $offerCount <= 0){

                        $order->update([ 'status' => 'refuse_by_system'  ]);
                        $order->offers->each->delete();

                    }else{
                        $order->update([ 'status' => 'pending'  ]);
                    }
                $order->update([ 'time_out' => 1 ]);

                $notify = $this->notify->NotificationDbType(8,$order->user_id,null, null,$order);
                foreach ($order['user']['devices'] as $device):

                    $this->push->sendPushNotification((array) $device['device'], null, $notify['title'], $notify['body'],
                        [
                            'id'            => $notify['id'],
                            'offer'         => $offerCount,
                            'orderId'       => $notify['order_id'],
                            'type'          => $notify['type'],
                            'title'         => $notify['title'],
                            'body'          => $notify['body'],
                            'created_at'    => $notify['created_at'],
                        ]
                    );

                endforeach;

            endif;

        endforeach;
    }


}
