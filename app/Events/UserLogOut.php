<?php

namespace App\Events;


class UserLogOut
{


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $user;
    public $request ;

    public function __construct( $user , $request)
    {
        $this->user     = $user;
        $this->request  = $request;
    }
}
