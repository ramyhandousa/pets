<?php

namespace App\Events;

class uploadImage extends Event
{

    public $model;
    public $request;
    public function __construct($model , $request)
    {
        $this->model = $model;
        $this->request = $request;
    }
}
