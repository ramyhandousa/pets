<?php

namespace App\Events;


class ContactUs
{


    public $user;
    public $sender;
    public $request;
    public function __construct($user,$sender,$request)
    {

        $this->user = $user;
        $this->sender = $sender;
        $this->request = $request;
    }


}
