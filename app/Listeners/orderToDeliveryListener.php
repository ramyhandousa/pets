<?php

namespace App\Listeners;

use App\Events\sendOrderToDelivery;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\Notification;
use App\Models\OrderOffer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class orderToDeliveryListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  sendOrderToDelivery  $event
     * @return void
     */
    public function handle(sendOrderToDelivery $event)
    {
        $user = $event->request->user();

        $title = ' طلبات جديدة *';
        $body = 'يوجد طلب توصيل جديد  ';
        $type = 3 ;

        $deliveries =  $user->notifications->sendToDelivery($title,$body ,$user->id,$type,$event->order->id);

        $orderOffers = collect($this->mappingOffer($deliveries['ids'], $event->order))->toArray();

        OrderOffer::insert($orderOffers);

        Notification::insert(collect($deliveries['notifications'])->toArray());

        $this->push->sendPushNotification($deliveries['devices'], null, $title, $body,
            [  'type' => $type, 'title' => $title, 'body' => $body ]
        );
    }



    public function mappingOffer($ids, $order){

        return collect($ids)->map(function ($delivery_id) use ($order) {
            return [
                'delivery_id' => $delivery_id,
                'order_id' => $order->id,
            ];
        });
    }

}
