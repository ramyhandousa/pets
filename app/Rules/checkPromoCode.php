<?php

namespace App\Rules;

use App\Models\PromoCode;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class checkPromoCode implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return PromoCode::whereIsSuspend(0)->whereIsDeleted(0)->where('end_at','>=',now())->whereCode($value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'لأسف هذا الكود غير صالح للإستخدام';
    }
}
