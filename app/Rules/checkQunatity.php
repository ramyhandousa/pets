<?php

namespace App\Rules;

use App\Models\Pet;
use App\Models\Supplie;
use Illuminate\Contracts\Validation\Rule;

class checkQunatity implements Rule
{

    public $type;
    public $petId;
    public $supplieId;

    public function __construct($type, $petId , $supplieId )
    {
        $this->type         = $type;
        $this->petId        = $petId;
        $this->supplieId    = $supplieId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       if ($this->type == 'pets' && $this->petId){

           $pet = Pet::find($this->petId);

           if ($pet){
               return $value > $pet->quantity ? false : true;
           }


       }else{

           if ($this->supplieId){

               $supplie = Supplie::find($this->supplieId);

               if ($supplie){
                   return $value > $supplie->quantity ? false : true;
               }

           }

       }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'نعتذر ولكن الكمية غير كافية';
    }
}
