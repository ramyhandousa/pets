<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 9:11 AM
 */

namespace App\Scoping\Scopes;
use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OrderStatusScope implements Scope{


    public function apply(Builder $builder , $value){

        if ($value == 'new'){

            $data = $builder->whereStatus('new');

        }elseif ($value == 'pending'){

            $data = $builder->whereStatus('pending');

        }elseif ($value ==  'current'){

            $data = $builder->whereStatus('current');

        }elseif ($value ==  'finish'){

            $data = $builder->whereIn('status',['refuse','finish','refuse_by_system']);

        }else{
            $data = $builder;
        }

        return $data;
    }

}
