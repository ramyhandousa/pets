<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class LocationScope implements Scope
{

    public function apply(Builder $builder, $value)
    {
        $lat    =  request()->latitude ;
        $lang   =  request()->longitude ;

        if ($lat || $lang){
            return $builder->select(DB::raw('*, ( 3959  * acos( cos( radians('.$lat.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$lang.') )
                                                                                        + sin( radians('.$lat.') ) * sin( radians( latitude ) ) ) ) AS distance'))
//                ->having('distance', '<',50)
                ->orderBy('distance');
        }

    }
}
