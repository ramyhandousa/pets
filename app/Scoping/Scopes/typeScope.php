<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class typeScope implements Scope {


    public function apply(Builder $builder , $value){

        if ($value == 'animal'){
            return    $builder->where('definition',$value);

        }else{

            return    $builder->where('definition',$value);
        }

    }
}
