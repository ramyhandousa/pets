<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class filterCategoryList implements Scope
{

    public function apply(Builder $builder , $value){

//        $filter =  filter_var($request->supplies,FILTER_VALIDATE_BOOL);
//
//        if (  $filter ){
//            $lists->where('parent_id', 2 );
//        }else{
//            $lists->where('parent_id', 1 );
//        }

        if (in_array($value,['animal','animal_supplies','plant','plant_supplies'])){

            return    $builder->whereType($value);
        }

    }

}
