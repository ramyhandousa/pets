<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 9:11 AM
 */

namespace App\Scoping\Scopes;
use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OrderDeliveryStatusScope implements Scope{


    public function apply(Builder $builder , $value){

       if ($value ==  'current'){

            $data = $builder->whereStatus('current');

       }elseif ($value ==  'finish'){

            $data = $builder->whereIn('status',['refuse','finish']);

       }else{
            $data = $builder;
       }

        return $data;
    }

}
