<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OrderOfferDeliveryStatusScope implements Scope
{

    public function apply(Builder $builder, $value)
    {
        if ($value == 'new'){

            $data = $builder->whereNull('delivery_price')->where('ignore','=',0)->whereHas('order',function ($order){
                $order->where('status' ,'new');
            });

        }elseif ($value == 'pending'){

            $data = $builder->whereNotNull('delivery_price')->where('ignore','=',0)->whereHas('order',function ($order){
                $order->whereIn('status' ,['new','pending']);
            });

        }else{
            $data = $builder;
        }

        return $data;
    }
}
