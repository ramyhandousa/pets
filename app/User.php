<?php

namespace App;

use App\Models\Device;
use App\Models\MasterUser;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Pet;
use App\Models\Profile;
use App\Models\Rate;
use App\Models\VerifyUser;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function master_user(){
        return $this->belongsTo(MasterUser::class);
    }

    public function code(){
        return $this->hasOne(VerifyUser::class);
    }

    public function profile(){
        return $this->hasOne(Profile::class);
    }

    public function pets(){
        return $this->hasMany(Pet::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function notifications(){
        return $this->hasMany(Notification::class);
    }
    public function devices(){
        return $this->hasMany(Device::class);
    }

    public function rate_delivery(){
        return $this->hasMany(Rate::class,'delivery_id');
    }

    public function rate_delivery_count(){
        $count =  $this->hasMany(Rate::class,'delivery_id')->average('rate');

        return $count ?  number_format($count,2) : (string) 0;
    }


}
