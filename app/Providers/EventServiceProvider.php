<?php

namespace App\Providers;

use App\Events\ContactUs;
use App\Events\phoneChange;
use App\Events\sendOrderToDelivery;
use App\Events\uploadImage;
use App\Events\UserLogOut;
use App\Listeners\ContactUsListener;
use App\Listeners\orderToDeliveryListener;
use App\Listeners\phoneChangeListener;
use App\Listeners\uploadImageListener;
use App\Listeners\UserLogOutListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        phoneChange::class => [
            phoneChangeListener::class
        ],
        uploadImage::class => [
            uploadImageListener::class
        ],
        UserLogOut::class => [
            UserLogOutListener::class
        ],
        ContactUs::class => [
            ContactUsListener::class
        ],
        sendOrderToDelivery::class => [
            orderToDeliveryListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
