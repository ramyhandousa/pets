<?php

namespace App\Providers;

use App\Models\Notification;
use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) {

            $helper = new \App\Http\Helpers\Images();
            $setting = new Setting();
            $auth = Auth::guard('master_user');
            $master_user = $auth->user();

//            dd($master_user);
//            dd($master_user->abilities);
            $notification_system = $auth->check() ? $master_user->admin ?  optional($master_user->admin)->notifications : optional($master_user->helper_admin)->notifications : [];

            $view->with(compact('master_user','helper','setting','notification_system'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}


