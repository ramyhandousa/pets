<?php

namespace App\Providers;

use App\Observers\UserObserver;
use App\User;
use Illuminate\Support\ServiceProvider;
use \Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);

        Validator::extend('percentage', function ($attribute, $value, $parameters, $validator) {

            $sum = 0;
            foreach ($parameters as $key => $attributeName) {
                $attributeValue = array_get($validator->getData(), $attributeName);
                $sum += floatval($attributeValue);
            }
            $sum += floatval($value);

            return $sum <= 100;
        });
    }
}
