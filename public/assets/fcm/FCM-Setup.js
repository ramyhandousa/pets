$.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
 var config = {
     apiKey: "AIzaSyBLhw1iNE5BVugNFBQmFJTn_ZFR51BdlOQ",
     authDomain: "from-zero-c8cec.firebaseapp.com",
     databaseURL: "https://from-zero-c8cec.firebaseio.com",
     projectId: "from-zero-c8cec",
     storageBucket: "from-zero-c8cec.appspot.com",
     messagingSenderId: "680321186358",
     appId: "1:680321186358:web:2e966efeefdf64a1f07697",
     measurementId: "G-PS7N7N1Q9X"
 };
  firebase.initializeApp(config);

const messaging = firebase.messaging();



messaging.usePublicVapidKey("BLrAGV7VGHiiHEL8sYBKnNSTffiIEy9BTSRx3t2EUMtVqys6-bRHJwUgLYXfE7ijsHIDCtydETrPmo_hPJEEmB4");

messaging.requestPermission().then(function () {

    // console.log('Notification permission granted.');


    getToken();

}).catch(function (err) {

    // console.log('Unable to get permission to notify.', err);

});

function getToken() {

    messaging.onTokenRefresh(function () {
        messaging.getToken().then(function (refreshedToken) {
            console.log(refreshedToken);
            Listen();
        }).catch(function (err) {
            console.log('Unable to retrieve refreshed token ', err);
        });
    });

    messaging.getToken().then(function (currentToken) {

        if (currentToken) {
            console.log(currentToken);
            if (userId) {
                $.ajax({
                    type: "POST", url: url, data: {id: userId, token: currentToken}, success: function (data) {
                    }
                });
            }

            Listen();

        } else {

            console.log('No Instance ID token available. Request permission to generate one.');
        }

    }).catch(function (err) {

        console.log('An error occurred while retrieving token. Ramy ', err);

    });


}



function Listen() {

    messaging.onMessage(function (payload) {

         var n = new Notification(payload.notification.title, {
			body: payload.notification.body,
			icon: payload.notification.icon, // optional
			onclick: payload.notification.click_action
		});

        console.log(payload)
        n.onclick = function(event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
              window.open(payload.notification.click_action, '_blank');
            }
    });
}
