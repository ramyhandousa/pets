<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    $path = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path1 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path2 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path3 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path4 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    return [
        'id_photo'      => $path,
        'license'       => $path1,
        'form_picture'  => $path2,
        'car_inside'    => [$path1 , $path2],
        'car_all'       => [$path1 , $path2, $path3, $path4],
    ];
});
