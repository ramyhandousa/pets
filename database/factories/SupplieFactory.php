<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Supplie;
use Faker\Generator as Faker;

$factory->define(Supplie::class, function (Faker $faker) {
    $names = ['دراي فؤد' ,'إكسسوارات','طوق قطة'];
    $random_keys =  array_rand($names,3);
    $path = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path1 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path2 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path3 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path4 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';

    return [
        'name'          =>  $names[$random_keys[0]],
        'desc'          => $faker->text,
        'city_id'       =>  rand(1,9),
        'category_id'   => rand(6,8),
        'price'         => array_rand($names),
        'quantity'      => rand(10,50),
        'latitude'      => '31.045813',
        'longitude'     => '31.353786',
        'address'       => 'فرينش فرايز',
        'images'        => [$path , $path1 , $path2, $path3, $path4],
    ];
});
