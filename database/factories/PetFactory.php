<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Pet;
use Faker\Generator as Faker;

$factory->define(Pet::class, function (Faker $faker) {

    $names = ['لونا' ,'كلوي','بيلا','لوسي','ليلي','صوفي','لولا','زوي'];
    $random_keys =  array_rand($names,3);
    $path = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path1 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path2 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path3 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';
    $path4 = 'http://lorempixel.com/500/50'.rand(1,9).'/sports/';

    return [
        'name'          =>  $names[$random_keys[0]],
        'desc'          => $faker->text,
        'city_id'       => 8,
        'category_id'   => 3,
        'color_id'      => rand(1,10),
        'price'         => array_rand($names),
        'age'           => rand(1,5),
        'quantity'      => rand(10,50),
        'weight'        => rand(1,5),
        'latitude'      => '31.037701 ',
        'longitude'     => '31.355880',
        'address'       => 'StoneYard Restaurant',
        'images'        => [$path , $path1 , $path2, $path3, $path4],
        'video'         => 'https://www.youtube.com/watch?v=9xwazD5SyVg'
    ];
});
