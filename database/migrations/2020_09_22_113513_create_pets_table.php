<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->enum('definition',['animal', 'plant'])->default('animal');
            $table->enum('type',['male', 'female'])->default('male');
            $table->string('name');
            $table->text('desc');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('color_id');
            $table->decimal('price');
            $table->tinyInteger('age')->unsigned();
            $table->integer('quantity')->unsigned();
            $table->string('weight');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('address');
            $table->string('images');
            $table->string('video');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('color_id')->references('id')->on('colors') ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pets');
    }
}
