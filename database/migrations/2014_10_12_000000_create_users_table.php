<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_user',['admin', 'user', 'delivery'])->default('user');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('image',100)->nullable();
            $table->string('api_token',60);
            $table->string('password')->nullable();
            $table->boolean('is_active')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->boolean('is_suspend')->default(0);
            $table->string('message')->nullable();
            $table->string('lang',6)->default('ar');
            $table->string('bank_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('account_name')->nullable();
            $table->string('iban_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
