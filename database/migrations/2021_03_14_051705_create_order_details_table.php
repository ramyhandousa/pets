<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('order_id');
            $table->enum('type',['pet', 'plant'])->default('pet');
            $table->boolean('is_supplie')->default(0);
            $table->unsignedBigInteger('general_id');
            $table->unsignedBigInteger('quantity');
            $table->unsignedBigInteger('category_id')->nullable();

            $table->foreign('order_id')->references('id')->on('orders') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
