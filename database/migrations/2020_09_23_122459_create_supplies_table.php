<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->id();
            $table->enum('definition',['animal', 'plant'])->default('animal');
            $table->string('name');
            $table->text('desc');
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('category_id');
            $table->integer('quantity')->unsigned();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('address');
            $table->decimal('price');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
