<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['pets', 'supplies'])->default('pets');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('delivery_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('pet_id')->nullable();
            $table->integer('quantity')->unsigned();
            $table->tinyInteger('is_pay')->default('0');
            $table->tinyInteger('time_out')->default('0');
            $table->decimal('price');
            $table->decimal('delivery_price')->nullable();
            $table->enum('payment',['made','stc','credit_card'])->default('credit_card');
            $table->string('paymentId')->nullable();
            $table->enum('status', ['new','pending', 'current','finish','refuse_by_system'])->default('new');
            $table->string('message')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('delivery_id')->references('id')->on('users') ->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories') ->onDelete('cascade');
            $table->foreign('pet_id')->references('id')->on('pets') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
