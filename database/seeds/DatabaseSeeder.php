<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);


//        factory(App\User::class, 1)->create()->each(function ($user) {
////            $user->profile()->save(factory(\App\Models\Profile::class)->make());
//            $user->pets()->save(factory(\App\Models\Pet::class)->make());
//        });
        factory(\App\Models\Supplie::class, 1)->create();

    }
}
