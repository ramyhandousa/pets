@extends('admin.layouts.master')
@section('title', __('trans.banks_management'))


@section('styles')
    <style>
        {{--position:relative;--}}
    /*.progress {  width:100%; height: 30px; border: 1px solid #7F98B2; padding: 1px; border-radius: 3px; }*/
        /*    .bar { background-color: #B4F5B4; width:0%; height:25px; border-radius: 3px; }*/
        /*    .percent { position:absolute; display:inline-block; top:3px; left:48%; color: #7F98B2;}*/
    </style>
@endsection
@section('content')



    <form id="my_form_submit" accept-charset="utf-8" method="POST" action="{{ route('cultural_videos.update', $video->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-lg-12  ">
                <div class="btn-group pull-right m-t-15">
                    {{--  <a href="{{ route('users.create') }}" type="button" class="btn btn-custom waves-effect waves-light"
                        aria-expanded="false"> @lang('maincp.add')
                         <span class="m-l-5">
                         <i class="fa fa-plus"></i>
                     </span>
                     </a> --}}
                </div>
                <h4 class="page-title">  إضافة فيديو  </h4>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12  ">
                <div class="card-box">

                    <div class="row">

                        <div   class="col-xs-8">

                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="userName">عنوان للفيديو*</label>
                                    <input type="text" name="title"  value="{{$video->title}}"
                                           class="form-control requiredFieldWithMaxLenght"
                                           required  placeholder="إسم الفيديو..."/>
                                    <p class="help-block" id="error_userName"></p>

                                </div>
                            </div>

                            <div class="col-xs-12 ">
                                <div class="form-group">
                                    <label for="userName"> وصف الفيديو *</label>
                                    <textarea type="text" name="desc" class="form-control m-input title "
                                              required placeholder="وصف الفيديو "   >{{ $video->desc}}</textarea>
                                </div>
                            </div>

                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="userName"> لينك الفيديو (إختياري)</label>
                                    <input type="url" class="form-control m-input  url " name="url"  value="{{$video->url}}" pattern="https?://.+"   />
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-4">

                            <div class="form-group changeElement">
                                <label for="usernames">الفيديو *</label>

                                <video id="videoPlayer" width="100%" height="50%" controls>
                                    <source src="{{URL("/")."/" .$video->file}}" type="video/mp4">
                                </video>
                            </div>

                        </div>
                        <!-- end col -->
                        @if($video->image)
                            <div class="col-xs-4 changeElement">
                                <div class="form-group">
                                    <label for="usernames">صورة للفيديو     (إختياري)  </label>
                                    <input type="file"
                                           data-default-file="{{URL("/")."/" .$video->image}}" data-show-remove="false" disabled
                                           data-errors-position="outside"
                                           accept="image/*"  name="image" class="dropify" />
                                </div>

                            </div>
                        @endIf
                        <div style="display: none" id="checkForVideo"  class="col-xs-4 changeElement">
                            <div class="form-group">
                                <label for="usernames">الفيديو *</label>
                                <input type="file"  id="myUploader"  accept="video/*"  name="file" class="dropify" />
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow=""
                                         aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                        0%
                                    </div>
                                </div>
                                <br />
                                <div id="success">

                                </div>
                            </div>
                            <label for="image">رفع صورة ( إختياري )</label>
                            <input type="file"  accept="image/*"  name="image" class="dropify" />
                        </div>

                        <!-- end col -->

                        <div class="form-group text-right m-t-20">
                            <div class="form-group text-right m-t-20 changeElement">
                                <a onclick="changeVideo()" class="btn btn-warning waves-effect waves-light m-t-20"   >
                                    تغير الفيديو
                                </a>
                            </div>

                            <div style="display: none" class="form-group text-right m-t-20 changeElement">
                                <a onclick="changeVideo()" class="btn btn-warning waves-effect waves-light m-t-20 "     >
                                    رجوع  للفيديو القديم
                                </a>
                            </div>
                            <button class="btn btn-warning waves-effect waves-light m-t-20 hideButton" value="Submit"   type="submit">
                                @lang('maincp.save_data')
                            </button>
                            <button onclick="window.history.back();return false;" type="reset"
                                    class="btn btn-default waves-effect waves-light m-l-5 m-t-20">
                                @lang('maincp.disable')
                            </button>
                        </div>
                    </div>
                </div>




            </div>
        </div><!-- end col -->


        </div>
        <!-- end row -->
    </form>

@endsection



@section('scripts')

    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>--}}
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <script type="text/javascript"
            src="{{ request()->root() }}/assets/admin/js/validate-{{ config('app.locale') }}.js"></script>


    <script type="text/javascript">

        var  checkForVideo = document.getElementById("checkForVideo");


        function changeVideo() {

            $(".changeElement").toggle();

        }


        var fileSize = true;

        function validate( ) {
            var value = $("#myUploader").val();

            if ( !value && checkForVideo.style.display !== 'none' ) {
                showErrors('{{ session()->get('errors', 'برجاء اختيار فيديو التحميل من فضلك  ') }}');
                return false;
            }

            return true;
        }

        $('#myUploader').bind('change', function() {

            const file = Math.round((this.files[0].size / 1024));
            if (file > 20000) {
                fileSize = false;
                showErrors('{{ session()->get('errors', 'برجاء اختيار فيديو حجم أقل من فضلك  ') }}');

            }else {
                fileSize = true;
            }

        });



        $(document).ready(function(){


            $('form').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this),
                    form = $(this);

                form.parsley().validate();

                if (form.parsley().isValid() && validate() && fileSize ){

                    $.ajax({
                        beforeSend :  function (){
                            $('.loading').show()
                            console.log("her is coming")
                        },
                        xhr: function() {
                            var xhr = new window.XMLHttpRequest();

                            xhr.upload.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total;
                                    percentComplete = parseInt(percentComplete * 100);

                                    $('.progress-bar').width(percentComplete+'%');
                                    $('.progress-bar').html(percentComplete+'%');

                                }
                            }, false);

                            return xhr;
                        },
                        type: 'POST',
                        url: $(this).attr('action'),
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success:function(data)
                        {
                            messageDisplay( 'نجاح' ,data.message )
                            setTimeout(function () {
                                window.location.href = data.url;
                            }, 2000);
                        },
                        error :function (data){
                            var message = data.responseJSON.error[0];
                            errorMessageTostar(  "خطأ", message)
                        },
                        complete: function (){
                            $('.loading').hide();
                        }
                    });

                }else {
                    console.log("here is error")
                }


            });


        });


        function messageDisplay($title, $message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            $toastlast = toastr[shortCutFunction](msg, title);
        }

        function errorMessageTostar($title, $message) {
            var shortCutFunction = 'error';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            var $toast = toastr[shortCutFunction](msg, title);
            $toastlast = $toast;
        }
    </script>
@endsection
