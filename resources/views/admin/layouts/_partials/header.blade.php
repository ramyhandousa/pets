<!-- Navigation Bar-->
<header id="topnav">

    <div class="topbar-main">
        <div class="container">

            <!-- LOGO -->
            <div class="topbar-left">
                {{--<a href="{{ route('admin.home') }}" class="logo" style="width: 150px;">--}}
                    {{--<img style="width: 100%" src="{{ request()->root() }}/public/assets/admin/images/logo.png"></a>--}}
            </div>
            <!-- End Logo container-->


            <div class="menu-extras">

                <ul class="nav navbar-nav navbar-right pull-right">
                    {{--<li>--}}
                    {{--<forsm role="search" class="navbar-left app-search pull-left hidden-xs">--}}
                    {{--<input type="text" placeholder="بحث ..." class="form-control">--}}
                    {{--<a href=""><i class="fa fa-search"></i></a>--}}
                    {{--</form>--}}
                    {{--</li>--}}

                    <li>
                        <!-- Notification -->
                        <div class="notification-box">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <a href="javascript:void(0);" class="right-bar-toggle">
                                        <i class="zmdi zmdi-notifications-none"></i>
                                    </a>
                                    <div class="noti-dot">
                                        <span class="dot"></span>
                                        <span class="pulse"></span>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <!-- End Notification bar -->
                    </li>


                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown"
                           aria-expanded="true">

                            @if(isset($helper))
                                @if($master_user->admin)
                                    <img src="{{  $helper->getDefaultImage(URL('/').'/'. optional($master_user->admin)->image ,'/assets/admin/images/default.png') }}"
                                         alt="user-img" class="img-circle user-img">
                                @else
                                    <img src="{{  $helper->getDefaultImage(URL('/').'/'. optional($master_user->helper_admin)->image,'/assets/admin/images/default.png') }}"
                                         alt="user-img" class="img-circle user-img">
                                @endif

                            @endif
                        </a>

                        <ul class="dropdown-menu">
                            <li><a href="{{ route('helpAdmin.edit', $master_user->id)}}"><i
                                            class="ti-user m-r-5"></i>@lang('maincp.personal_page')</a></li>
{{--                            <!--<li><a href="{{ route('users.edit', auth()->id()) }}"><i class="ti-settings m-r-5"></i>-->--}}
                            <!--        @lang('global.settings')-->
                            <!--    </a>-->
                            <!--</li>-->
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off m-r-5"></i>@lang('maincp.log_out')
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>

        </div>
    </div>

    <form id="logout-form" action="{{ route('administrator.logout') }}" method="POST"
          style="display: none;">
        {{ csrf_field() }}
    </form>

    <div class="navbar-custom">
        <div class="container">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu" style="    font-size: 14px;">



                    @if(auth('master_user')->user()->checkIfHasAbility('statistics_manage'))
                        <li>
                            <a href="{{ route('admin.home') }}"><i class="zmdi zmdi-view-dashboard"></i>
                                <span> @lang('menu.home') </span> </a>
                        </li>
                    @endif

                    @if(auth('master_user')->user()->checkIfHasAbility('app_users_manage'))

                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-layers"></i><span>  @lang('trans.managers_ctrl_panel') </span>
                                </a>
                                <ul class="submenu ">
                                    <li><a href="{{ route('helpAdmin.index') }}">  مساعدين مدير التطبيق </a></li>
                                    {{--                                @can('permission_manage')--}}
                                    <li>
                                        <a href="{{route('roles.index')}}">  @lang('trans.roles_and_permission')     </a>
                                    </li>
                                    {{--                                @endcan--}}

                                </ul>
                            </li>
                    @endif

                    @if(auth('master_user')->user()->checkIfHasAbility('users_manage'))

                        <li class="has-submenu">
                            <a href="{{ route('users.index') }}"><i
                                        class="zmdi zmdi-accounts"></i><span>إدارة المستخدمين   </span>
                            </a>
                        </li>

                    @endif


                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-layers"></i><span>  إدارة المناديب   </span>
                            </a>
                            <ul class="submenu ">
                                <li><a href="{{ route('deliveries.index') }}?is_accepted=0"> طلبات جديدة    </a></li>
                                <li>  <a href="{{route('deliveries.index')}}">  المفعلين لدينا   </a>  </li>

                            </ul>
                        </li>

                        <li class="has-submenu">
                            <a href="#"><i class="zmdi zmdi-layers"></i><span>  إدارة الإضافات الخاصة بالتطبيق   </span>
                            </a>
                            <ul class="submenu ">
                                <li><a href="{{ route('cities.index') }}"> المدن  </a></li>
                                <li>  <a href="{{route('categories.index')}}">    الأقسام </a>  </li>
                                <li>  <a href="{{route('supplies.index')}}">    المستلزمات </a>  </li>
                                <li>  <a href="{{route('plants.index')}}">    النباتات </a>  </li>
                                <li>  <a href="{{route('pets.index')}}">    الحيوانات </a>  </li>
                                <li>  <a href="{{route('cultural_videos.index')}}">    فيديوهات ثقافية </a>  </li>
                                <li>  <a href="{{route('promo_codes.index')}}">   كوبونات الخصم </a>  </li>
                                <li>  <a href="{{ route('notifications_admin.promo_code') }}"> إرسال  كوبونات الخصم </a>  </li>


                            </ul>
                        </li>

                        @if(auth('master_user')->user()->checkIfHasAbility('settings_manage'))

                            <li class="has-submenu">
                                <a href="#"><i class="zmdi zmdi-settings"></i><span>@lang('maincp.setting')<i
                                            class="fa fa-arrow-down visible-xs" aria-hidden="true"></i></span> </a>
                                <ul class="submenu">
                                    <li><a href="{{ route('settings.contactus') }}">@lang('trans.general_setting_app')</a>    </li>
                                    <li><a href="{{ route('settings.minutes_control') }}">التحكم في الوقت الخاص بالعروض </a>    </li>
                                    <li><a href="{{ route('settings.aboutus') }}">@lang('trans.about_app')</a>   </li>
                                    <li><a href="{{ route('settings.terms') }}">@lang('trans.terms') </a></li>
{{--                                    <li><a href="{{ route('banks.index') }}"> الحسابات البنكية </a></li>--}}
{{--                                    <li><a href="{{ route('bank-transfer-admin') }}"> إدارة التحويلات البنكية  </a></li>--}}
{{--                                    <li><a href="{{ route('public.notifications') }}"> الإشعارات الجماعية      </a></li>--}}
                                    <li><a href="{{ route('admin_contact_us_inbox.index') }}">  تواصل معنا     </a></li>
                                    <li><a href="{{ route('admin.support.reports') }}"> البلاغات   </a></li>
                                </ul>
                            </li>
                    @endif

                        <li class="has-submenu">

                            <a href="{{route("reports.index")}}"><i class="zmdi zmdi-settings"></i><span>إدارة الطلبات
                                    <i  class="fa fa-arrow-down visible-xs" aria-hidden="true"></i></span>
                            </a>
                            <ul class="submenu">
                                <li><a href="{{ route('reports.index') }}?type=pets">طلبات الحيوانات</a>    </li>
                                <li><a href="{{ route('reports.index') }}?type=pets_supplies">طلبات مستلزمات الحيوانات </a>    </li>
                                <li><a href="{{ route('reports.index') }}?type=plants">طلبات النباتات </a>    </li>
                                <li><a href="{{ route('reports.index') }}?type=plants_supplies">طلبات مستلزمات النباتات </a>    </li>
                            </ul>
                        </li>
                        </li>

                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>

</header>
<!-- End Navigation Bar-->


<div class="wrapper">
    <div class="container">
