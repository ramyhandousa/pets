@extends('admin.layouts.master')
@section("styles")

    <!-- Custom box css -->
    <link href="/assets/admin/plugins/custombox/dist/custombox.min.css" rel="stylesheet">
@endsection
@section('content')


    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">

                <button type="button" class="btn btn-custom  waves-effect waves-light"
                        onclick="window.history.back();return false;"> رجوع <span class="m-l-5"><i
                            class="fa fa-reply"></i></span>
                </button>

            </div>
            <h4 class="page-title">بيانات الطلب</h4>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-8">
            <div class="card-box">

                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="userName">رقم الطلب</label>
                        <input type="text" name="name" value="{{  $order->id  }}" class="form-control" readonly />
                    </div>
                </div>

                @if( optional($order->user)->name )
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">إسم المستخدم صاحب الطلب</label>
                            <input type="text" name="name" value="{{ optional($order->user)->name  }}" class="form-control" readonly />
                        </div>
                    </div>

                @endif

                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="userName">رقم الجوال   صاحب الطلب</label>
                        <input type="text" name="name" value="{{ optional( optional($order->user)->master_user)->phone  }}" class="form-control" readonly />
                    </div>
                </div>

                @if($order->delivery_id  )


                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">إسم المندوب  </label>
                            <input type="text"  class="form-control" readonly value="{{ optional($order->delivery)->name ? : "لا يوجد لديه إسم" }}"/>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">رقم  المندوب  </label>
                            <input type="text"  class="form-control" readonly   value="{{ optional( optional($order->delivery)->master_user)->phone ? : "لا يوجد حاليا" }}"/>
                        </div>
                    </div>

                @endif

                @if($order->delivery_price)
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">سعر التوصيل </label>
                            <input type="text" name="name" value="{{  $order->delivery_price   }}" class="form-control" readonly />
                        </div>
                    </div>
                @endif

                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="userName">سعر الطلب    </label>
{{--                        <input type="text" name="name" value="{{  $order->total_price - ( $order->discount_code ? : 0)  }}" class="form-control" readonly />--}}
                        <input type="text" name="name" value="{{  $order->price  }}" class="form-control" readonly />
                    </div>
                </div>

                @if($order->discount_code)
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">نسبة الخصم     </label>
                            <input type="text" name="name" value="{{ $order->discount_value   }}" class="form-control" readonly />
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">سعر الخصم     </label>
                            <input type="text" name="name" value="{{ $order->discount_code   }}" class="form-control" readonly />
                        </div>
                    </div>

                    <div class="col-xs-4">
                        <div class="form-group">
                            <label for="userName">إجمالي سعر الطلب     </label>
                            <input type="text" name="name" value="{{  $order->total_price   }}" class="form-control" readonly />
                        </div>
                    </div>
                @endif



                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="userName">طريقة دفع قيمة الطلب     </label>
                        <input type="text" name="name" value="{{ $order->payment == "cash" ? "كاش" : "اون لاين" }}" class="form-control" readonly />
                    </div>
                </div>


                @if($order->address)
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">عنوان للطلب</label>
                            <textarea class="form-control" readonly  cols="30" rows="4">{{  $order->address  }}</textarea>
                        </div>
                    </div>
                @endif



                <div class="col-xs-12">
                    <div class="form-group">
                        <label for="userName">حالة الطلب</label>
                        <input class="form-control" readonly value="{{  $order->status_translation  }}"  type="text">
                    </div>
                </div>


                @if(in_array($order->status , ["refuse_delivery","refuse_user"]) && $order->message)
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="userName">سبب الرفض    </label>
                            <input type="text" name="name" value="{{ $order->message }}" class="form-control" readonly />
                        </div>
                    </div>
                @endif


                <div class="row">

                </div>

            </div>
        </div><!-- end col -->


        <div class="col-md-4">
            <div class="card-box">
                <div class="row">
                    @if($order->pet_id != null   )

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">إسم {{$section_name}}  </label>
                                <input type="text" name="name" value="{{  optional($order->pet)->name  }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">مدينة {{$section_name}}  </label>
                                <input type="text" name="name" value="{{  optional( optional($order->pet)->city)->name_ar  }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">  قسم{{$section_name}}  </label>
                                <input type="text" name="name" value="{{ optional(optional($order->pet)->category)->name_ar  }}" class="form-control" readonly />
                            </div>
                        </div>
                    @else

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">إسم {{$section_name}}  </label>
                                <input type="text" name="name" value="{{  optional($order->supplie)->name  }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName">مدينة {{$section_name}}  </label>
                                <input type="text" name="name" value="{{  optional( optional($order->supplie)->city)->name_ar  }}" class="form-control" readonly />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="userName"> قسم   {{$section_name}}  </label>
                                <input type="text" name="name" value="{{  optional(optional($order->supplie)->category)->name_ar  }}" class="form-control" readonly />
                            </div>
                        </div>
                    @endif

                </div>

            </div>
        </div>

    </div>
    <!-- end row -->


@endsection

@section("scripts")


    <!-- Modal-Effect -->
    <script src="/assets/admin/plugins/custombox/dist/custombox.min.js"></script>
    <script src="/assets/admin/plugins/custombox/dist/legacy.min.js"></script>

    <script>
        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            swal({
                title: "هل انت متأكد؟",
                text: "سوف يتم رفض هذا الطلب.",
                type: "error",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        beforeSend: function (){
                            $('.loading').show();
                            $(".hide_any_action").hide();
                        },
                        url: url,
                        data: {id: id},
                        dataType: 'json',
                        success: function (data) {


                            messageDisplay( 'نجاح' ,data.message )
                            setTimeout(function () {
                                window.location.href = data.url;
                            }, 1000);

                        },
                        error:function (){

                            $(".hide_any_action").show();
                        },
                        complete:function (){
                            $('.loading').hide();
                        }
                    });
                }
            });
        });

        function messageDisplay($title, $message) {
            var shortCutFunction = 'success';
            var msg = $message;
            var title = $title;
            toastr.options = {
                positionClass: 'toast-top-left',
                onclick: null
            };
            $toastlast = toastr[shortCutFunction](msg, title);
        }

    </script>
@endsection
