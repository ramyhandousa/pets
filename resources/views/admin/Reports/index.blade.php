@extends('admin.layouts.master')
@section('title', 'إدارة الإشتراكات ')
@section('content')

    <!-- Page-Title -->
    <div class="row zoomIn">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">


            </div>
            <h4 class="page-title"> قائمة     {{$pageName}} </h4>
        </div>
    </div>


    <div class="row zoomIn">

        <div class="col-sm-12">
            <div class="card-box rotateOutUpRight ">

                <table id="datatable-responsive" class="table  table-striped">
                    <thead>
                    <tr>
                        <th>رقم الطلب  </th>
                        <th>اسم / رقم صاحب الطلب  </th>
                        <th>اسم / رقم  مندوب الطلب  </th>
                        @if(request('type') == "pets" || request('type') == "pets_supplies" )
                            <th>إسم الحيوان     </th>
                            <th>مدينة الحيوان     </th>
                            <th>قسم الحيوان     </th>
                        @else
                            <th>إسم النبات     </th>
                            <th>مدينة النبات     </th>
                            <th>قسم النبات     </th>
                        @endif


                        <th>كمية الطلب    </th>
                        <th>  الطلب مدفوع ام لا   </th>
                        <th>      تفاصيل    </th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($reports as $report)

                        <tr>
                            <td> {{  $report->id }}  </td>
                            <td> {{ optional($report->user)->name ? :  optional( optional($report->user)->master_user)->phone}}  </td>
                            <td> {{ optional($report->delivery)->name ? :  optional( optional($report->delivery)->master_user)->phone ? : "لا يوجد حاليا"}}  </td>
                            @if(request('type') == "pets" || request('type') == "pets_supplies" )
                                <td> {{ optional($report->pet)->name}}  </td>
                                <td> {{ optional( optional($report->pet)->city)->name_ar ? : "لا يوجد حاليا"}}  </td>
                                <td> {{ optional(optional($report->pet)->category)->name_ar ? : "لا يوجد حاليا"}}  </td>
                            @else
                                <td> {{ optional($report->supplie)->name ? : "لا يوجد حاليا"}}  </td>
                                <td> {{ optional( optional($report->supplie)->city)->name_ar ? : "لا يوجد حاليا"}}  </td>
                                <td> {{ optional(optional($report->supplie)->category)->name_ar ? : "لا يوجد حاليا"}}  </td>
                            @endif

                            <td> {{ $report->quantity }}  </td>
                            <td> {{ $report->is_pay == 0 ? "مدفوع" : "غير مدفوع" }}  </td>

                            <td>
                                <a href="{{ route('reports.show', $report->id) }}"
                                    data-toggle="tooltip" data-placement="top"
                                    data-original-title="@lang('institutioncp.show_details')"
                                    class="btn btn-icon btn-xs waves-effect  btn-info">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>


                        </tr>

                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- End row -->
@endsection

@section('scripts')

    <script>
        $(document).ready(function () {
            $('#datatable-responsive').DataTable( {
                "order": [[ 0, "desc" ]]
            } );

        });
    </script>


@endsection

