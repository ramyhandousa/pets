@extends('admin.layouts.master')
@section('title', __('maincp.user_data'))
@section('content')


    <form method="POST" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data"
          data-parsley-validate novalidate>
    {{ csrf_field() }}
    {{ method_field('PUT') }}



    <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="btn-group pull-right m-t-15">
                    {{--<button type="button" class="btn btn-custom  waves-effect waves-light"--}}
                        {{--onclick="window.history.back();return false;"> @lang('maincp.back') <span class="m-l-5"><i--}}
                                {{--class="fa fa-reply"></i></span>--}}
                    {{--</button>--}}

                    <a href="{{ route('users.index') }}"
                       class="btn btn-custom  waves-effect waves-light">
												<span><span>رجوع  </span>
													<i class="fa fa-reply"></i>
												</span>
                    </a>

                </div>
                <h4 class="page-title">@lang('maincp.user_data') </h4>
            </div>
        </div>


        <div class="row">


                <div class="col-sm-12">

                <div class="card-box">
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="card-box p-b-0">


                                <h4 class="header-title m-t-0 m-b-30">التفاصيل</h4>

                                <form>
                                    <div id="basicwizard" class=" pull-in">
                                        <ul class="nav nav-tabs navtab-wizard nav-justified bg-muted">
                                            <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="false">البيانات الاساسية</a></li>
                                            @if($user->profile)
                                                <li ><a href="#tab2" data-toggle="tab" aria-expanded="false"> الفرعية  </a></li>
                                            @endif
                                            <li ><a href="#tab3" data-toggle="tab" aria-expanded="false"> الحسابات البنكية  </a></li>
                                        </ul>
                                        <div class="tab-content b-0 m-b-0">
                                            <div class="tab-pane m-t-10 fade active in" id="tab1">
                                                <div class="row">
                                                    <div class="col-sm-8">
                                                        <div class="col-xs-12 col-lg-12">
                                                            <h4>@lang('maincp.personal_data')</h4>
                                                            <hr>
                                                        </div>

                                                        <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                            <label>@lang('maincp.full_name') :</label>
                                                            <input class="form-control" value="{{ $user->name }}"><br>
                                                        </div>

                                                        @if( optional($user->master_user)->phone )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.mobile_number') :</label>
                                                                <input class="form-control" value="{{ optional($user->master_user)->phone }}"><br>
                                                            </div>
                                                        @endif

                                                        @if( optional($user->master_user)->email )
                                                            <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                <label>@lang('maincp.e_mail')  :</label>
                                                                <input class="form-control" value="{{ optional($user->master_user)->email }}"><br>
                                                            </div>
                                                        @endif

                                                    </div>



                                                    <div class="col-sm-4">
                                                        <div class="card-box">
                                                            <div class="row">

                                                                <div class="card-box" style="overflow: hidden;">
                                                                    <h4 class="header-title m-t-0 m-b-30">@lang('institutioncp.personal_image')</h4>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( $user->image, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( $user->image, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </div>
                                            @if($user->profile)

                                                <div class="tab-pane m-t-10 fade  in" id="tab2">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="col-xs-12 col-lg-12">
                                                                <h4>البيانات الفرعية</h4>
                                                                <hr>
                                                            </div>

                                                                <label>     الصور المرفقة :</label>
                                                                <div class="card-box" style="overflow: hidden;">

                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الهوية</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( request()->root().'/' .optional($user->profile)->id_photo, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( request()->root().'/' .optional($user->profile)->id_photo, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الرخصة</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage(request()->root().'/' . optional($user->profile)->license, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( request()->root().'/' .optional($user->profile)->license, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <h4 class="header-title m-t-0 m-b-30">صورة الإستمارة</h4>
                                                                        <div class="col-sm-3">
                                                                            <a data-fancybox="gallery"
                                                                               href="{{ $helper->getDefaultImage( request()->root().'/' .optional($user->profile)->form_picture, request()->root().'/default.png') }}">
                                                                                <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                     src="{{ $helper->getDefaultImage( request()->root().'/' . optional($user->profile)->form_picture, request()->root().'/default.png') }}"/>
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="card-box">
                                                                <div class="row">

                                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                        <label>     صور السيارة من الداخل  :</label>

                                                                        @if(count(optional($user->profile)->car_inside) > 0)
                                                                            <div class="form-group">
                                                                                @foreach(optional($user->profile)->car_inside as $image)

                                                                                    <div class="col-sm-3">
                                                                                        <a   data-fancybox="gallery"
                                                                                             href="{{ $helper->getDefaultImage(  $image, request()->root().'/default.png') }}">
                                                                                            <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                                 src="{{ $helper->getDefaultImage(   $image, request()->root().'/default.png') }}"/>
                                                                                        </a>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        @endif

                                                                    </div>
                                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                                        <label>     صور السيارة من جميع الإتجهات  :</label>

                                                                        @if(count(optional($user->profile)->car_all) > 0)
                                                                            <div class="form-group">
                                                                                @foreach(optional($user->profile)->car_all as $image)

                                                                                    <div class="col-sm-3">
                                                                                        <a   data-fancybox="gallery"
                                                                                             href="{{ $helper->getDefaultImage(  $image, request()->root().'/default.png') }}">
                                                                                            <img class="img" style="width: 200px;height: 200px;object-fit: cover;border-radius: 10px;"
                                                                                                 src="{{ $helper->getDefaultImage(   $image, request()->root().'/default.png') }}"/>
                                                                                        </a>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        @endif

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            @endif

                                            <div class="tab-pane m-t-10 fade  in" id="tab3">
                                                <div class="row">
                                                    <h3>الحسابات البنكية</h3>

                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                        <label>إسم البنك   :</label>
                                                        <input class="form-control" value="{{ $user->bank_name ? : "لا يوجد" }}"><br>
                                                    </div>

                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                        <label>  رقم الحساب    :</label>
                                                        <input class="form-control" value="{{ $user->account_number ? : "لا يوجد" }}"><br>
                                                    </div>

                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                        <label>رقم الإيبان  :</label>
                                                        <input class="form-control" value="{{ $user->iban_number ? : "لا يوجد" }}"><br>
                                                    </div>

                                                    <div class="col-lg-12 col-xs-12 col-md-12 col-sm-12">
                                                        <label>إسم صاحب الحساب  :</label>
                                                        <input class="form-control" value="{{ $user->account_name ? : "لا يوجد" }}"><br>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

        </div>


    </form>

@endsection

