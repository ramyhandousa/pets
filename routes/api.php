<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::group([
    'namespace' => 'Api',
    'prefix' => 'v1'
], function () {

    Route::group([ 'namespace' => 'Delivery' ,'prefix' => 'Delivery'],function (){

        Route::group(['prefix' => 'Auth'], function () {

            Route::post('login','AuthController@login');
            Route::post('sign-up', 'AuthController@register');
            Route::post('forgetPassword', 'AuthController@forgetPassword');
            Route::post('resetPassword', 'AuthController@resetPassword');
            Route::post('checkCode', 'AuthController@checkCodeActivation');
            Route::post('checkCodeCorrect', 'AuthController@checkCodeCorrect');
            Route::post('resendCode', 'AuthController@resendCode');
            Route::post('changPassword', 'AuthController@changPassword');
            Route::post('editProfile', 'AuthController@editProfile');
            Route::post('edit_car', 'AuthController@edit_car');
            Route::post('logOut','AuthController@logOut');

            Route::post('upload-image','AuthController@upload');
            Route::post('remove-image','AuthController@removeImage');
            Route::post('remove-user','AuthController@removeUser');
        });


        Route::resource('orders','OrderDeliveryController');
        Route::post('orders/makeOffer/{order}','OrderDeliveryController@makeOffer');
        Route::post('orders/offerIgnore/{order}','OrderDeliveryController@offerIgnore');
        Route::post('orders/finish/{order}','OrderDeliveryController@orderFinish');
        Route::post('orders/time_out/{order}','OrderDeliveryController@time_out');

    });


    Route::group([ 'namespace' => 'User' , 'prefix' => 'User'], function () {

        Route::group(['prefix' => 'Auth'], function () {

            Route::post('sign-up', 'AuthController@register');
            Route::post('checkCode', 'AuthController@checkCodeActivation');
            Route::post('resendCode', 'AuthController@resendCode');
            Route::post('logOut','AuthController@logOut');
        });

        Route::group(['prefix' => 'home'], function () {

            Route::get('pets','HomeController@homePets');
            Route::get('supplies','HomeController@homeSupplie');
            Route::get('supplies/{supplie}','HomeController@showSupplie');

        });

        Route::resource('pets','PetsController');
        Route::resource('plants','PlantsController');

        Route::resource('orders','OrderUserController');
        Route::get('orders/listOffers/{order}','OrderUserController@listOffers');
        Route::post('orders/refuse_order/{order}','OrderUserController@refuseOrder');
        Route::post('orders/rate_order/{order}','OrderUserController@rateOrder');

    });

    Route::get("my_wallet","WalletController@my_wallet");
    Route::post("update_bank_account","WalletController@bank_account");


    Route::group(['prefix' => 'lists'], function () {

        Route::get('categories','ListController@categories');
        Route::get('colors','ListController@colours');
        Route::get('cities','ListController@cities');
        Route::get("videos","ListController@videos");

    });


    Route::group(['prefix' => 'payment'], function () {

        Route::get('success/{orderOffer}','PaymentController@success');
        Route::get('error','PaymentController@error');

    });

    Route::group(['prefix' => 'setting'], function () {

        Route::get('global_social','SettingController@global_social');
        Route::get('aboutUs','SettingController@aboutUs');
        Route::get('terms','SettingController@terms_user');
        Route::get('typesSupport','SettingController@getTypesSupport');
        Route::post('contact_us','SettingController@contact_us');
        Route::post('report','SettingController@report');
    });


    Route::get("list_country","TestController@list_country");
    Route::get("code_country","TestController@code_country");
    Route::get("prices_shipment","TestController@prices_shipment");
    Route::get("add_shipment","TestController@AddShipment");
    Route::get("read_shipping","TestController@read_shipping");
    Route::get("get_status_shipping","TestController@get_status_shipping");
    Route::get("can_be_cancel","TestController@can_be_cancel");
    Route::get("cancel_shipment","TestController@cancelShipment");

});


