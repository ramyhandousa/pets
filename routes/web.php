<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Auth::routes();



Route::get('/', 'HomeController@index')->name('home');

//Route::get('lang/{language}', 'LanguageController@switchLang')->name('lang.switch');

Route::group(['prefix' => 'administrator' , 'namespace' => 'Admin'], function () {

    Route::get('/login', 'LoginController@login')->name('admin.login');
    Route::post('/login', 'LoginController@postLogin')->name('admin.postLogin');

    // Password Reset Routes...

    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('administrator.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('administrator.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('administrator.password.reset.token');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

});


Route::group(['prefix' => 'administrator', 'namespace' => 'Admin', 'middleware' => ['admin']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('admin.home');


    Route::resource('helpAdmin', 'HelpAdminController');
    Route::get('helpAdmin/{id}/delete', 'HelpAdminController@delete')->name('helpAdmin.delete');
    Route::post('helpAdmin/{id}/delete', 'HelpAdminController@deleteHelpAdmin')->name('helpAdmin.message.delete');
    Route::post('helpAdmin/{id}/suspend', 'HelpAdminController@suspendHelpAdmin')->name('helpAdmin.message.suspend');
    Route::get('user/{id}/delete', 'UsersController@delete')->name('user.for.delete');
    Route::post('user/suspend', 'HelpAdminController@suspend')->name('user.suspend');
    // Roles routes ..........
    Route::resource('roles', 'RolesController');
    Route::post('role/delete', 'RolesController@delete')->name('role.delete');


    Route::resource('users', 'UsersController');
    Route::resource('providers_app', 'ProviderController');
    Route::post('providers_app/suspend', 'ProviderController@suspend')->name('providers_app.suspend');
    Route::post('accepted','UsersController@accpetedUser')->name('user.accepted');
    Route::post('refuseUser','UsersController@refuseUser')->name('user.refuseUser');
    Route::post('reAcceptUser','UsersController@reAcceptUser')->name('user.reAcceptUser');
    Route::post('suspendUser','UsersController@suspendUser')->name('user.suspendUser');

    Route::resource('deliveries', 'DeliveryController');
    Route::post('deliveries/accepted','DeliveryController@accpetedUser')->name('deliveries.accepted');
    Route::post('deliveries/refuseUser','DeliveryController@refuseUser')->name('deliveries.refuseUser');

    Route::resource('supplies', 'SuppliesController');
    Route::post('supplies/delete', 'SuppliesController@delete')->name('supplies.delete');
    Route::post('supplies/delete_image', 'SuppliesController@deleteImage')->name('supplies.deleteImage');

    Route::resource('pets', 'PetController');
    Route::resource('plants', 'PlantController');
    Route::post('pets/suspend', 'PetController@suspend')->name('pets.suspend');


    Route::resource('cultural_videos', 'CulturalVideoController');
    Route::post('cultural_videos/suspend', 'CulturalVideoController@suspend')->name('cultural_videos.suspend');
    Route::post('cultural_videos/delete', 'CulturalVideoController@delete')->name('cultural_videos.delete');


    Route::get('public/notifications', 'NotificationsController@publicNotifications')->name('public.notifications');
    Route::get('public/notifications/create', 'NotificationsController@createPublicNotifications')->name('create.public.notifications');

    Route::get('notifications/promo_code', 'NotificationsController@promo_code')->name('notifications_admin.promo_code');
    Route::get('notifications/get_promo_code', 'NotificationsController@get_promo_code')->name('notifications_admin.get_promo_code');
    Route::get('notifications/renderUsesData', 'NotificationsController@renderUsesData')->name('notifications_admin.renderUsesData');
    Route::post('notifications_admin/send_promo_code', 'NotificationsController@send_promo_code')->name('notifications_admin.send_promo_code');
    Route::post('notifications_admin/delete', 'NotificationsController@delete')->name('notifications_admin.delete');

    Route::post('send/public/notifications', 'NotificationsController@sendPublicNotifications')->name('send.public.notifications');


    // --------------------------------------  promo_codes .................
    Route::resource('promo_codes', 'PromoCodesController');
    Route::post('promo_codes/delete', 'PromoCodesController@delete')->name('promo_codes.delete');
    Route::post('promo_codes/suspend', 'PromoCodesController@suspend')->name('promo_codes.suspend');


    Route::get('settings/aboutus', 'SettingsController@aboutus')->name('settings.aboutus');
    Route::get('settings/taxs', 'SettingsController@taxs')->name('settings.taxs');
    Route::get('settings/termsProvider', 'SettingsController@termsGym')->name('settings.termsGym');
    Route::get('settings/terms', 'SettingsController@terms')->name('settings.terms');
    Route::get('settings/suspendElement', 'SettingsController@suspendElement')->name('settings.suspendElement');

    Route::get('/settings/app-general-settings', 'SettingsController@appGeneralSettings')->name('settings.app.general');
    Route::get('settings/contacts', 'SettingsController@contactus')->name('settings.contactus');
    Route::get('settings/minutes_control', 'SettingsController@minutes_control')->name('settings.minutes_control');


    Route::post('/settings', 'SettingsController@store')->name('administrator.settings.store');

//    Route::post('contactus/reply/{id}', 'SupportsController@reply')->name('support.reply');
//    Route::get('contactus', 'SupportsController@index')->name('support.index');
//    Route::get('contactus/{id}', 'SupportsController@show')->name('support.show');
//    Route::post('support/contact/delete', 'SupportsController@delete')->name('support.contact.delete');


//    Route::resource('supports', 'SupportsController');
//    Route::post('supports/delete', 'SupportsController@delete')->name('supports.delete');

//    Route::resource('types', 'TypesSupportController');


    Route::post('city/delete/group', 'CitiesController@groupDelete')->name('cities.group.delete');
    Route::post('cities/delete', 'CitiesController@delete')->name('city.delete');
    Route::resource('cities', 'CitiesController');
    Route::post('city/suspend', 'CitiesController@suspend')->name('city.suspend');

    // -------------------------------------- categories .................
    Route::resource('categories', 'CategoriesController');
    Route::post('categories/delete', 'CategoriesController@delete')->name('categories.delete');
    Route::post('categories/suspend', 'CategoriesController@suspend')->name('categories.suspend');

    Route::resource('reports', 'ReportsController');

    Route::get('/testImageView', 'UsersController@testImageView')->name('Provider.testImageView');
    Route::post('/testImage', 'UsersController@testImage')->name('Provider.testImage');


    Route::resource('banks', 'BanksController');
    Route::post('bank/suspend', 'BanksController@suspend')->name('bank.suspend');

    Route::get('bank-transfer-admin', 'BankTransferController@index')->name('bank-transfer-admin');
    Route::post('accept-transfer-admin', 'BankTransferController@accepted')->name('accept-transfer-admin');
    Route::post('refuse-transfer-admin', 'BankTransferController@refuse')->name('refuse-transfer-admin');

    Route::resource('admin_contact_us_inbox', 'ContactUsController');
    Route::get("admin_reports","ContactUsController@reports")->name("admin.support.reports");
    Route::get('updateIsRead', 'ContactUsController@updateIsRead')->name('admin.support.updateIsRead');
    Route::get('updateIsDeleted', 'ContactUsController@updateIsDeleted')->name('admin.support.updateIsDeleted');
    Route::get('removeAllMessages', 'ContactUsController@removeAllMessages')->name('admin.support.removeAllMessages');


    Route::resource('BankProjects', 'BankProjectsController');
    Route::post('BankProjects/accepted', 'BankProjectsController@accepted')->name('BankProjects.accepted');
    Route::post('BankProjects/refuse', 'BankProjectsController@refuse')->name('BankProjects.refuse');

    Route::resource('reports', 'ReportsController');
    Route::get('provider_data_report', 'ReportsController@gym_data')->name('provider_data_report');

    Route::post('/logout', 'LoginController@logout')->name('administrator.logout');

});



Route::get('/sub', function (Illuminate\Http\Request $request) {

    $cities =\App\Models\City::whereParentId($request->id)->get();

    if (!empty($cities) && count($cities) > 0){
        return response()->json( $cities);
    }else{

        return response()->json(401);
    }


})->name('getSub');



Route::get('roles', function () {

    $user = Auth::guard('master_user')->user();
//    $user->retract('admin');
    $user->assign('*');
    Bouncer::allow('*')->everything();
    $user->allow('users_manage');
});


Route::post('user/update/token', function (Illuminate\Http\Request $request) {

    $user = \App\User::whereId($request->id)->first();

    if ($request->token) {
        $data = \App\Models\Device::where('device', $request->token)->first();
        if ($data) {
            $data->user_id = $user->id;
            $data->save();
        } else {


            $data = new \App\Models\Device;
            $data->device = $request->token;
            $data->user_id = $user->id;
            $data->device_type = 'web';
            $data->save();
        }
    }

})->name('user.update.token');



Route::get('testing',function (){
    $data = [];
    $data[] =  env('DB_DATABASE');
    $data[] =  env('DB_USERNAME');
    $data[] =  env('DB_PASSWORD');
    $data[] =    \Illuminate\Support\Facades\App::environment();

    return $data;
});
